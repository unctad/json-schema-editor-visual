const JSONPATH_JOIN_CHAR = '.';
exports.JSONPATH_JOIN_CHAR = JSONPATH_JOIN_CHAR;
exports.lang = 'en_US';
exports.langData = {};
exports.format = [
    {name: 'date-time'},
    {name: 'date'},
    {name: 'email'},
    {name: 'hostname'},
    {name: 'ipv4'},
    {name: 'ipv6'},
    {name: 'uri'}
];
const _ = require('underscore');
exports.SCHEMA_TYPE_MULTIPLE = {
    'array_of_file': 'file',
    'file': 'array_of_file',
    'catalog': 'array_of_catalog',
    'array_of_catalog': 'catalog'
}
exports.SCHEMA_TYPE = ['object', 'array', 'string', 'number', 'boolean', 'date', 'datetime', 'array_of_file', 'catalog'];
exports.LOCKED_FIELD_TYPE = {
    string: ['date', 'datetime', 'number'],
    date: ['string', 'datetime', 'number'],
    datetime: ['string', 'date', 'number'],
    number: ['string', 'date', 'datetime'],
};

exports.defaultSchema = {
    string: {
        type: 'string'
    },
    number: {
        type: 'number'
    },
    array: {
        type: 'array',
        items: {
            type: 'object',
            properties: {
                field_1: {
                    type: 'string',
                }
            },
            required: ['field_1']
        }
    },
    object: {
        type: 'object',
        properties: {
            'field_1': {
                type: 'string',
            }
        }
    },
    boolean: {
        type: 'boolean'
    },
    integer: {
        type: 'integer'
    },
    date: {
        type: 'string',
        format: 'date'
    },
    datetime: {
        type: 'string',
        format: 'date-time'
    },
    file: {
        type: 'file',
        consumes: [
            "application/pdf"
        ]
    }
};

// 防抖函数，减少高频触发的函数执行的频率
// 请在 constructor 里使用:

// this.func = debounce(this.func, 400);
exports.debounce = (func, wait) => {
    let timeout;
    return function () {
        clearTimeout(timeout);
        timeout = setTimeout(func, wait);
    };
};

function getData(state, keys) {
    let curState = state;
    for (let i = 0; i < keys.length; i++) {
        curState = curState[keys[i]];
    }
    return curState;
}

exports.getData = getData;

exports.setData = function (state, keys, value) {
    let curState = state;
    for (let i = 0; i < keys.length - 1; i++) {
        curState = curState[keys[i]];
    }
    curState[keys[keys.length - 1]] = value;
};

exports.deleteData = function (state, keys) {
    let curState = state;
    for (let i = 0; i < keys.length - 1; i++) {
        curState = curState[keys[i]];
    }

    delete curState[keys[keys.length - 1]];
};

exports.getParentKeys = function (keys) {
    if (keys.length === 1) return [];
    let arr = [].concat(keys);
    arr.splice(keys.length - 1, 1);
    return arr;
};

exports.clearSomeFields = function (keys, data) {
    const newData = Object.assign({}, data);
    keys.forEach(key => {
        delete newData[key];
    });
    return newData;
};

function getFieldstitle(data) {
    const requiredtitle = [];
    Object.keys(data).map(title => {
        requiredtitle.push(title);
    });

    return requiredtitle;
}

function handleSchemaRequired(schema, checked) {
    if (schema.type === 'object') {
        let requiredtitle = getFieldstitle(schema.properties);

        // schema.required = checked ? [].concat(requiredtitle) : [];
        if (checked) {
            schema.required = [].concat(requiredtitle);
        } else {
            delete schema.required;
        }

        handleObject(schema.properties, checked);
    } else if (schema.type === 'array') {
        handleSchemaRequired(schema.items, checked);
    } else {
        return schema;
    }
}

function handleObject(properties, checked) {
    for (var key in properties) {
        if (properties[key].type === 'array' || properties[key].type === 'object')
            handleSchemaRequired(properties[key], checked);
    }
}

exports.handleSchemaRequired = handleSchemaRequired;

function cloneObject(obj) {
    if (typeof obj === 'object') {
        if (Array.isArray(obj)) {
            var newArr = [];
            obj.forEach(function (item, index) {
                newArr[index] = cloneObject(item);
            });
            return newArr;
        } else {
            var newObj = {};
            for (var key in obj) {
                newObj[key] = cloneObject(obj[key]);
            }
            return newObj;
        }
    } else {
        return obj;
    }
}

exports.cloneObject = cloneObject;

exports.textFormat = function (str, data) {
    for (const k in data) {
        str = str.replace("{" + k + "}", data[k])
    }
    return str;
};


exports.validateSchema = function (schema) {
    if(typeof schema !== 'object' || schema == null || typeof schema.type !== 'string') {
        return false;
    }
    if(schema.type === 'object') {
        if(typeof schema.properties !== 'object' || schema.properties == null) {
            return false;
        }
        for (const key of Object.keys(schema.properties)) {
            if(!exports.validateSchema(schema.properties[key]) ){
                return false;
            }
        }
    }
    else if(schema.type === 'array') {
        if(!exports.validateSchema(schema.items)) {
            return false;
        }
    }
    return true;
}

exports.getSchemaRealType = function (schema) {
    if (schema.type === 'string') {
        if (schema.format === 'date-time') {
            return 'datetime';
        } else if (schema.format === 'date') {
            return 'date'
        }
    } else if (schema.type === 'array') {
        if (schema.items && schema.items.type === 'file') {
            return 'array_of_file';
        }
    }
    return schema.type;
}
