import React from 'react';
import {Component} from 'react'
import CreatableSelect from 'react-select/creatable';
import cn from 'classnames';

export default class MultiSelect extends Component{
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: [],
            inputValue: "",
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        let selectedOption = nextProps.selectedOption;
        if(selectedOption) {
            selectedOption = MultiSelect.fixSelectedOptions(selectedOption);
        }
        return {selectedOption: selectedOption || []}
    }

    static fixSelectedOptions(selectedOptions) {
        let selectedOption = JSON.parse(JSON.stringify(selectedOptions));
        selectedOption.forEach((option, optionKey) => {
            if(!option['$value']) {
                option['$value'] = option.value;
            }
            option.value = optionKey;
        });
        return selectedOption;
    }

    handleInputChange = (inputValue) => {
        this.setState({inputValue});
    };

    handleChange = selectedOption => {
        if (!selectedOption) {
            selectedOption = [];
        } else {
            selectedOption = MultiSelect.fixSelectedOptions(selectedOption);
        }
        if(this.props.onChange) {
            this.props.onChange(selectedOption.map(o => {
                let s = {...o}
                s.value = o['$value'];
                delete s['$value'];
                return s;
            }));
        }
        this.setState({selectedOption});
    };

    handleKeyDown = (event) => {
        const {inputValue, value} = this.state;
        switch (event.key) {
            case 'Enter':
            case 'Tab':
                if (!inputValue) {
                    return;
                }
                this.setState({
                    inputValue: '',
                });
                this.handleChange([...this.state.selectedOption, {value: inputValue, label: inputValue}]);
                event.preventDefault();
                break;
            case ' ':
                if (!inputValue.length) {
                    this.setState({
                        inputValue: ' ',
                    });
                }
                break;
        }
    };

    render() {
        const options = this.props.options || [];
        return (
            <CreatableSelect
                value={this.state.selectedOption}
                onChange={this.handleChange}
                options={options}
                isMulti={true}
                hideSelectedOptions={false}
                onKeyDown={this.handleKeyDown}
                inputValue={this.state.inputValue}
                onInputChange={this.handleInputChange}
                menuIsOpen={options.some(s => s.label.toLowerCase().includes(this.state.inputValue.toLowerCase())) ? undefined : false}
                placeholder={this.props.placeholder}
                className={cn("multi-select", "type-select-style")}
                classNamePrefix="react-select"
                isDisabled={this.props.disabled}
                menuPlacement={"auto"}
            />
        )
    }

}
