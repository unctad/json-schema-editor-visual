import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {Icon} from "antd";
import FieldInput from "./FieldInput";
import LocalProvider from "../LocalProvider";
import ReactSelect from "react-select";
import MultiSelect from "../MultiSelect";

export default class TriggerCondition extends PureComponent {
    static propTypes = {
        data: PropTypes.array,
        onChange: PropTypes.func,
        type: PropTypes.string,
        fields: PropTypes.array,
        additionalValueOptions: PropTypes.array
    };

    constructor(props) {
        super(props);
        this.state = {

        };
        this.gates = [
            {
                value: '&&',
                label: LocalProvider('trigger_condition_gate_and')
            },
            {
                value: '||',
                label: LocalProvider('trigger_condition_gate_or')
            }
        ];
        this.logics = [];
        if(this.props.type === 'integer' || this.props.type === 'number') {
            this.logics = [
                {
                    value: '==',
                    label: LocalProvider('trigger_condition_equals')
                },
                {
                    value: '!=',
                    label: LocalProvider('trigger_condition_not_equal')
                },
                {
                    value: '>',
                    label: LocalProvider('trigger_condition_greater_than')
                },
                {
                    value: '<',
                    label: LocalProvider('trigger_condition_less_than')
                },
                {
                    value: '*',
                    label: LocalProvider('trigger_condition_always')
                }
            ]
        }
        else if(this.props.type === 'string') {
            this.logics = [
                {
                    value: '==',
                    label: LocalProvider('trigger_condition_equals')
                },
                {
                    value: '!=',
                    label: LocalProvider('trigger_condition_not_equal')
                },
                {
                    value: 'contains',
                    label: LocalProvider('trigger_condition_contains')
                },
                {
                    value: '*',
                    label: LocalProvider('trigger_condition_always')
                }
            ]
        }
        else if(this.props.type === 'catalog' || this.props.type === 'array_of_catalog') {
            this.logics = [
                {
                    value: '==',
                    label: LocalProvider('trigger_condition_equals')
                },
                {
                    value: '!=',
                    label: LocalProvider('trigger_condition_not_equal')
                },
                {
                    value: 'contains',
                    label: LocalProvider('trigger_condition_contains')
                },
                {
                    value: '*',
                    label: LocalProvider('trigger_condition_always')
                }
            ]
        }
        else if(this.props.type === 'boolean') {
            this.logics = [
                {
                    value: '==',
                    label: LocalProvider('trigger_condition_equals')
                },
                {
                    value: '*',
                    label: LocalProvider('trigger_condition_always')
                }
            ]
        }
        else if(this.props.type === 'datetime' || this.props.type === 'date') {
            this.logics = [
                {
                    value: '==',
                    label: LocalProvider('trigger_condition_equals')
                },
                {
                    value: '!=',
                    label: LocalProvider('trigger_condition_not_equal')
                },
                {
                    value: '>',
                    label: LocalProvider('trigger_condition_greater_than')
                },
                {
                    value: '<',
                    label: LocalProvider('trigger_condition_less_than')
                },
                {
                    value: '*',
                    label: LocalProvider('trigger_condition_always')
                }
            ]
        }
        else {
            this.logics = [
                {
                    value: '*',
                    label: LocalProvider('trigger_condition_always')
                }
            ]
        }
        this.add = this.add.bind(this);
        this.additionalMustacheOptionsByType = {
            'date': [
                {
                    value: '|year|suffix-2',
                    label: ' | ' + LocalProvider('mustache_last_two_digits_year')
                }
            ],
            'datetime': [
                {
                    value: '|year|suffix-2',
                    label: ' | ' + LocalProvider('mustache_last_two_digits_year')
                }
            ]
        }
        this.fields = this.props.fields.map(field => ({...field, value: field.field_id ?  field.field_id: field.field}));
        this.valueOptions = props.additionalValueOptions ? [...props.additionalValueOptions] : [];
        this.props.fields.forEach(field => {
            if(field.field_id) {
                const baseMustache = `field-${field.field_id}`;
                const valueOption = {value: `{${baseMustache}}`, label: field.label ? field.label : field.path.join(' — ')};
                this.valueOptions.push(valueOption);
                if (field.type && this.additionalMustacheOptionsByType[field.type]) {
                    for (const mustacheOption of this.additionalMustacheOptionsByType[field.type]) {
                        this.valueOptions.push({
                            value: `{${baseMustache}${mustacheOption.value}}`,
                            label: `${valueOption.label}${mustacheOption.label}`
                        });
                    }
                }
            }
        });
    }

    render() {
        return (
            <table className={"schema-trigger-condition"}>
                <caption className="system-label">{LocalProvider('trigger_condition')}</caption>
                <tbody>
                {this.props.data.map((item, itemIndex) => {
                    const gate = this.gates.find(g => g.value === item.gate) || null;
                    const logic = this.logics.find(l => l.value === item.logic) || null;
                    return (
                        <tr key={itemIndex}>
                            <td className="trigger-condition-gate">
                                <div className="system-input-container select size-s value-m">
                                    <ReactSelect
                                        value={gate}
                                        onChange={this.handleChangeGate.bind(this, itemIndex)}
                                        options={this.gates}
                                        placeholder={""}
                                        isSearchable={false}
                                        menuPlacement={"auto"}
                                        className="type-select-style"
                                        classNamePrefix="react-select"
                                    />
                                </div>
                            </td>
                            <td className="trigger-condition-logic">
                                <div className="system-input-container select size-s value-m">
                                    <ReactSelect
                                        value={logic}
                                        onChange={this.handleChangeLogic.bind(this, itemIndex)}
                                        options={this.logics}
                                        placeholder={""}
                                        isSearchable={false}
                                        menuPlacement={"auto"}
                                        className="type-select-style"
                                        classNamePrefix="react-select"
                                    />
                                </div>
                            </td>
                            <td className="trigger-condition-value">
                                {item.logic !== '*' && this.renderValue(item.value, itemIndex)}
                            </td>
                            <td className="trigger-condition-actions">
                                <button type="button" className="system-button size-s minimal grayscale icon-button" onClick={this.remove.bind(this, itemIndex)}>
                                    <i className="fas fa-minus button-icon" />
                                </button>
                                <button type="button" className="system-button size-s minimal grayscale icon-button" onClick={this.add}>
                                    <i className="fas fa-plus button-icon" />
                                </button>
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        )
    }

    handleChangeGate(itemIndex, option) {
        this.props.data[itemIndex].gate = option.value;
        this.forceUpdate();
        this.props.onChange(this.props.data);
    }

    handleChangeLogic(itemIndex, option) {
        this.props.data[itemIndex].logic = option.value;
        this.forceUpdate();
        this.props.onChange(this.props.data);
    }

    add() {
        this.props.data.push({
            gate: '&&',
            logic: this.logics[0].value,
            value: ''
        });
        this.forceUpdate();
        this.props.onChange(this.props.data);
    }

    remove(index) {
        this.props.data.splice(index, 1);
        this.forceUpdate();
        this.props.onChange(this.props.data);
    }

    renderValue(value, itemIndex) {
        if(this.props.type === 'string' || this.props.type === 'integer' || this.props.type === 'number' || this.props.type === 'catalog' || this.props.type === 'array_of_catalog' || this.props.type === 'date' || this.props.type === 'datetime') {
            return (
                <div className="system-input-container select multiple size-s value-m">
                    <MultiSelect
                        selectedOption={this.getSelectedOptions(value)}
                        options={this.valueOptions}
                        onChange={this.onChangeMultiSelect.bind(this, itemIndex)}
                        placeholder={""}
                        className="type-select-style"
                        classNamePrefix="react-select"
                    />
                </div>
            );
        } else if (this.props.type === 'boolean') {
            const booleanOptions = [
                {label: 'true', value: 'true'},
                {label: 'false', value: 'false'}
            ];
            return (
                <div className="system-input-container select size-s value-m">
                    <ReactSelect
                        value={value ? booleanOptions[0] : booleanOptions[1]}
                        onChange={this.onChangeBooleanValue.bind(this, itemIndex)}
                        options={booleanOptions}
                        placeholder={""}
                        isSearchable={false}
                        menuPlacement={"auto"}
                        className="type-select-style"
                        classNamePrefix="react-select"
                    />
                </div>
            );
        }
        return "";
    }

    getSelectedOptions (value) {
        let selectedOptions = [];
        if(!value) {
            return selectedOptions;
        }
        const re = /({[\w|\-|_|\d]+})/;
        let matched = re.exec(value);
        while (matched) {
            const beforeValue = value.substring(0, matched.index);
            if(beforeValue.length > 0) {
                selectedOptions.push({value: beforeValue, label: beforeValue});
            }
            let option = {
                value: matched[0],
                label: matched[0]
            };
            let valueOption = this.valueOptions.find(o => o.value === option.value);
            if(valueOption) {
                option.label = valueOption.label;
            }
            selectedOptions.push(option);
            value = value.substring(matched.index + option.value.length);
            matched = re.exec(value);
        }
        if(value.length > 0) {
            selectedOptions.push({value, label: value});
        }
        return selectedOptions;
    }

    onChangeBooleanValue(itemIndex, option) {
        this.props.data[itemIndex].value = option.value === 'true';
        this.props.onChange(this.props.data);
    }

    onChangeMultiSelect(itemIndex, selected) {
        this.props.data[itemIndex].value = selected.map(s => s.value).join('');
        this.props.onChange(this.props.data);
    }

    onChangeValue(itemIndex, e) {
        this.props.data[itemIndex].value = e.target.value;
        this.props.onChange(this.props.data);
    }
}
