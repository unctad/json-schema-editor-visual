import React, {PureComponent} from 'react'
import cn from 'classnames';
import PropTypes from 'prop-types'
import {Col, InputNumber, Row} from "antd";
import LocalProvider from "../LocalProvider";
import MultiSelect from "../MultiSelect";
import ReactSelect from 'react-select'

export default class TriggerAction extends PureComponent {
    static propTypes = {
        data: PropTypes.array,
        fields: PropTypes.array,
        onChange: PropTypes.func,
        type: PropTypes.string,
        additionalValueOptions: PropTypes.array
    };

    constructor(props) {
        super(props);
        this.state = {

        };
        this.actionTypes = [
            {
                value: 'set-value',
                label: LocalProvider('trigger_action_set_value')
            },
            {
                value: 'upper-case',
                label: LocalProvider('trigger_action_upper_case')
            },
            {
                value: 'left-pad',
                label: LocalProvider('trigger_action_left_pad')
            },
            {
                value: 'right-pad',
                label: LocalProvider('trigger_action_right_pad')
            },
            {
                value: 'center-pad',
                label: LocalProvider('trigger_action_center_pad')
            },
            {
                value: 'random-number',
                label: LocalProvider('trigger_action_random_number')
            },
            {
                value: 'add-checksum',
                label: LocalProvider('trigger_action_add_checksum')
            }
        ];
        this.checksumAlgorithms = [
            {
                value: 'luhn',
                label: LocalProvider('algorithm_luhn')
            }
        ];
        this.additionalMustacheOptionsByType = {
            'date': [
                {
                    value: '|year|suffix-2',
                    label: ' | ' + LocalProvider('mustache_last_two_digits_year')
                }
            ],
            'datetime': [
                {
                    value: '|year|suffix-2',
                    label: ' | ' + LocalProvider('mustache_last_two_digits_year')
                }
            ]
        }
        this.fields = this.props.fields.map(field => ({...field, value: field.field_id ?  field.field_id: field.field}));
        this.valueOptions = props.additionalValueOptions ? [...props.additionalValueOptions] : [];
        this.props.fields.forEach(field => {
            if(field.field_id) {
                const baseMustache = `field-${field.field_id}`;
                const valueOption = {value: `{${baseMustache}}`, label: field.label ? field.label : field.path.join(' — ')};
                this.valueOptions.push(valueOption);
                if (field.type && this.additionalMustacheOptionsByType[field.type]) {
                    for (const mustacheOption of this.additionalMustacheOptionsByType[field.type]) {
                        this.valueOptions.push({
                            value: `{${baseMustache}${mustacheOption.value}}`,
                            label: `${valueOption.label}${mustacheOption.label}`
                        });
                    }
                }
            }
        });
    }

    render() {
        const fieldOptions = this.props.fields.map(f => ({value: f.field_id || f.field, label: f.label || f.path.join(' — ')}));
        return (
            <table className={"schema-trigger-action"}>
                <caption className="system-label">{LocalProvider('trigger_action')}</caption>
                <tbody>
                {this.props.data.map((item, itemIndex) => {
                    const fieldValue = item.field_id || item.field;
                    const fieldOption = fieldOptions.find(o => o.value === fieldValue) || null;
                    const actionType = this.actionTypes.find(o => o.value === item.type) || null;
                    return (
                        <tr key={itemIndex} className="trigger-action-item">
                            <td className="trigger-action-field-option">
                                <div className="system-input-container select size-s value-m">
                                    <ReactSelect
                                        value={fieldOption}
                                        onChange={this.handleChangeField.bind(this, itemIndex)}
                                        options={fieldOptions}
                                        placeholder={""}
                                        isSearchable={false}
                                        menuPlacement={"auto"}
                                        className="type-select-style"
                                        classNamePrefix="react-select"
                                    />
                                </div>
                            </td>
                            <td className={cn("trigger-action-type", {'full': item.type === 'upper-case'})}>
                                <div className="system-input-container select size-s value-m">
                                    <ReactSelect
                                        value={actionType}
                                        onChange={this.handleChangeActionType.bind(this, itemIndex)}
                                        options={this.actionTypes}
                                        placeholder={""}
                                        isSearchable={false}
                                        menuPlacement={"auto"}
                                        className="type-select-style"
                                        classNamePrefix="react-select"
                                    />
                                </div>
                            </td>
                            <td className="trigger-action-value">
                                {this.renderValue(item)}
                            </td>
                            <td className="trigger-action-actions">
                                <button type="button" className="system-button size-s minimal grayscale icon-button" onClick={this.remove.bind(this, itemIndex)}>
                                    <i className="fas fa-minus button-icon" />
                                </button>
                                <button type="button" className="system-button size-s minimal grayscale icon-button" onClick={this.add.bind(this, itemIndex)}>
                                    <i className="fas fa-plus button-icon" />
                                </button>
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        )
    }

    renderValue(item) {
        if(['set-value', 'left-pad', 'right-pad', 'center-pad'].indexOf(item.type) !== -1) {
            return <div className="system-input-container select multiple size-s value-m">
                <MultiSelect
                    selectedOption={this.getSelectedOptions(item.value)}
                    options={this.valueOptions}
                    onChange={this.onChangeMultiSelect.bind(this, item)}
                    placeholder={""}
                    className="type-select-style"
                    classNamePrefix="react-select"
                />
            </div>;
        }
        else if (item.type === 'random-number') {
            return (
                <Row type="flex" align="middle">
                    <Col span={9}>
                        <div className="system-input-container size-s value-m">
                            <InputNumber
                                value={item.minimum}
                                placeholder={LocalProvider('minimum')}
                                onChange={e => {
                                    item.minimum = e;
                                    if (item.minimum == null) {
                                        delete item.minimum;
                                    }
                                    this.props.onChange(this.props.data);
                                }}
                            />
                        </div>
                    </Col>
                    <Col span={1}>
                        <p className={"pt-2"}>—</p>
                    </Col>
                    <Col span={9}>
                        <div className="system-input-container size-s value-m">
                            <InputNumber
                                value={item.maximum}
                                placeholder={LocalProvider('maximum')}
                                onChange={e => {
                                    item.maximum = e;
                                    if (item.maximum == null) {
                                        delete item.maximum;
                                    }
                                    this.props.onChange(this.props.data);
                                }}
                            />
                        </div>
                    </Col>
                </Row>
            )
        }
        else if (item.type === 'add-checksum') {
            const algorithmOption = this.checksumAlgorithms.find(o => o.value === item.algorithm) || null;
            return (
                <ReactSelect
                    value={algorithmOption}
                    onChange={this.handleChangeChecksumAlgorithm.bind(this, item)}
                    options={this.checksumAlgorithms}
                    placeholder={""}
                    isSearchable={false}
                    menuPlacement={"auto"}
                    className="type-select-style"
                    classNamePrefix="react-select"
                />
            )
        }
        return "";
    }

    getSelectedOptions (value) {
        let selectedOptions = [];
        if(!value) {
            return selectedOptions;
        }
        const re = /({[\w|\-|_|\d]+})/;
        let matched = re.exec(value);
        while (matched) {
            const beforeValue = value.substring(0, matched.index);
            if(beforeValue.length > 0) {
                selectedOptions.push({value: beforeValue, label: beforeValue});
            }
            let option = {
                value: matched[0],
                label: matched[0]
            };
            let valueOption = this.valueOptions.find(o => o.value === option.value);
            if(valueOption) {
                option.label = valueOption.label;
            }
            selectedOptions.push(option);
            value = value.substring(matched.index + option.value.length);
            matched = re.exec(value);
        }
        if(value.length > 0) {
            selectedOptions.push({value, label: value});
        }
        return selectedOptions;
    }

    onChangeMultiSelect(item, selected) {
        item.value = selected.map(s => s.value).join('');
        this.props.onChange(this.props.data);
    }

    handleChangeActionType(itemIndex, option) {
        this.props.data[itemIndex].type = option.value;
        if(option.value === 'upper-case') {
            if(this.props.data[itemIndex].value !== undefined) {
                delete this.props.data[itemIndex].value;
            }
        }
        else if(this.props.data[itemIndex].value === undefined) {
            this.props.data[itemIndex].value = '';
        }
        else if (this.props.data[itemIndex].type === 'add-checksum') {
            if (!this.props.data[itemIndex].algorithm) {
                const o = this.checksumAlgorithms[0];
                this.props.data[itemIndex].algorithm = o.value;
            }
        }
        this.forceUpdate();
        this.props.onChange(this.props.data);
    }

    handleChangeField(itemIndex, option) {
        let item = this.props.data[itemIndex];
        if(typeof (option.value) === "number") {
            item.field_id = option.value;
            if(item.hasOwnProperty('field')) {
                delete item.field;
            }
        }
        else {
            item.field = option.value;
            if(item.hasOwnProperty('field_id')) {
                delete item.field_id;
            }
        }
        this.forceUpdate();
        this.props.onChange(this.props.data);
    }

    handleChangeChecksumAlgorithm(item, option) {
        item.algorithm = option.value;
        this.forceUpdate();
        this.props.onChange(this.props.data);
    }

    add(index) {
        this.props.data.push({...this.props.data[index]});
        this.forceUpdate();
        this.props.onChange(this.props.data);
    }

    remove(index) {
        this.props.data.splice(index, 1);
        this.forceUpdate();
        this.props.onChange(this.props.data);
    }
}
