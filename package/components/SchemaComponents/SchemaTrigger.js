import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import TriggerCondition from "./TriggerCondition";
import { Tooltip } from 'antd';
import TriggerAction from "./TriggerAction";
import LocalProvider from "../LocalProvider";

export default class SchemaTrigger extends PureComponent {
    static propTypes = {
        onChange: PropTypes.func,
        data: PropTypes.object,
        fields: PropTypes.array,
        additionalValueOptions: PropTypes.array
    };

    constructor(props) {
        super(props);
        this.state = {

        };
        this.addTrigger = this.addTrigger.bind(this);
    }

    render() {
        const {triggers, format} = this.props.data;
        let {type} = this.props.data;
        if (type === 'string') {
            if (format === 'date-time') {
                type = 'datetime';
            }
            else if (format === 'date') {
                type = 'date';
            }
        }
        else if (type === 'array') {
            if (this.props.data.items.type === 'catalog') {
                type = 'array_of_catalog';
            }
        }
        return (
            <div className="schema-trigger">
                {triggers && triggers.map((trigger, triggerIndex) => (
                    <div className="trigger-item" key={triggerIndex}>
                        <div className="remove-item-container">
                            <Tooltip placement="top"
                                title={LocalProvider('remove_trigger')}>
                                <button className="system-button size-s minimal color-error icon-button delete-trigger" onClick={this.removeTrigger.bind(this, triggerIndex)}>
                                    <i className="far fa-trash-can button-icon" />
                                </button>
                            </Tooltip>
                        </div>
                        <TriggerCondition fields={this.props.fields} additionalValueOptions={this.props.additionalValueOptions} onChange={this.onChangeCondition.bind(this, triggerIndex)} type={type} data={trigger.conditions}/>
                        <TriggerAction fields={this.props.fields} additionalValueOptions={this.props.additionalValueOptions} onChange={this.onChangeAction.bind(this, triggerIndex)} data={trigger.actions}/>
                    </div>
                ))}
                <button className="system-button size-s value-m outline" onClick={this.addTrigger}>
                    <i className="far fa-plus button-icon" />
                    {LocalProvider('add_trigger')}
                </button>
            </div>
        )
    }

    onChangeCondition(index) {
        if(this.props.data.triggers[index].conditions.length === 0) {
            this.removeTrigger(index);
        }
        this.props.onChange(this.props.data);
    }

    onChangeAction(index) {
        if(this.props.data.triggers[index].actions.length === 0) {
            this.removeTrigger(index);
        }
        this.props.onChange(this.props.data);
    }

    addTrigger() {
        let {data} = this.props;
        let {triggers} = data;
        if(!triggers) {
            triggers = [];
        }
        triggers.push({
            conditions: [
                {
                    logic: "==",
                    value: "",
                    gate: "&&"
                }
            ],
            actions: [
                this.getInitialAction()
            ]
        });
        data.triggers = triggers;
        this.props.onChange(data);
        this.forceUpdate();
    }

    getInitialAction() {
        const field = this.props.fields.find(f => f.field_id === this.props.data['$id']) || this.props.fields.find(f => f.field_id > this.props.data['$id']) || this.props.fields[0];
        return  {
            type: "set-value",
            value: "",
            field_id: field.field_id
        };
    }

    removeTrigger(index) {
        let {data} = this.props;
        let triggers = [...data.triggers];
        triggers.splice(index, 1);
        data.triggers = triggers;
        if(data.triggers.length === 0) {
            delete data.triggers;
        }
        this.props.onChange(data);
        this.forceUpdate();
    }
}
