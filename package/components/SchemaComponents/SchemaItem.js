import {Dropdown, Icon, Menu, Tooltip} from "antd";
import Select from 'react-select';
import FieldInput from "./FieldInput";
import LocaleProvider from "../LocalProvider";
import {SCHEMA_TYPE, LOCKED_FIELD_TYPE, SCHEMA_TYPE_MULTIPLE} from "../../utils";
import React from "react";
import cn from 'classnames';
import {ValueContainer as ValueContainerBase} from "react-select/animated";

export class SchemaItem extends React.Component {
    constructor(props) {
        super(props);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleEnableRequire = this.handleEnableRequire.bind(this);
        this.handleChangeType = this.handleChangeType.bind(this);
        this.handleChangeDesc = this.handleChangeDesc.bind(this);
        this.addSiblingNode = this.addSiblingNode.bind(this);
        this.addChildNode = this.addChildNode.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.clone = this.clone.bind(this);
        this.toggleMultiple = this.toggleMultiple.bind(this);
        this.fieldInputRef = null;
    }

    toggleMultiple(e) {
        e.preventDefault();
        e.stopPropagation();
        if (this.props.item.data.type === 'array') {
            const triggers = this.props.item.data.triggers;
            this.props.item.data = this.props.item.data.items;
            if (triggers) {
                this.props.item.data.triggers = triggers;
            }
        }
        else {
            const triggers = this.props.item.data.triggers;
            if (triggers) {
                delete this.props.item.data.triggers;
            }
            this.props.item.data = {
                type: 'array',
                items: this.props.item.data
            };
            if (triggers) {
                this.props.item.data.triggers = triggers;
            }
        }
        this.forceUpdate();
        this.props.onChange(this.props.item);
    }

    componentDidMount() {
        if(this.props.refX) {
            this.props.refX(this);
        }
    }

    componentWillUnmount() {
        if(this.props.refX) {
            this.props.refX(undefined);
        }
    }

    getDataType() {
        const {item} = this.props;
        if (item.data.type === 'string') {
            if (item.data.format === 'date-time') {
                return 'datetime';
            } else if (item.data.format === 'date') {
                return 'date';
            }
        } else if (item.data.type === 'integer') {
            return 'number'
        }
        else if(item.data.type === 'array') {
            if(item.data.items && item.data.items.type === 'file') {
                return 'array_of_file';
            }
            else if(item.data.items && item.data.items.type === 'catalog') {
                return 'array_of_catalog';
            }
        }
        return item.data.type;
    }

    render() {
        const {dragHandler, item, collapseIcon} = this.props;
        const type = this.getDataType();
        const typeItemComponents = {};
        if (SCHEMA_TYPE_MULTIPLE[type]) {
            typeItemComponents.ValueContainer = ({children, ...props}) => (
                <ValueContainerBase {...props}>
                    {children}
                    <Tooltip placement="top" title={LocaleProvider('field.toggle-multiple-tooltip')}>
                        <div className="multiple-control-badge" onMouseDown={this.toggleMultiple}>M</div>
                    </Tooltip>
                </ValueContainerBase>
            );
        }


        let keyInvalidMessage = null;
        if (this.hasAnotherSameKey()) {
            keyInvalidMessage = LocaleProvider('key-duplicated')
        } else if (this.hasReservedMatchedKey()) {
            keyInvalidMessage = LocaleProvider('key-reserved')
        }
        const typeOptions = this.getTypes().map(eachType => ({
            value: eachType,
            label: LocaleProvider(`schema_type_${eachType}`)
        }));
        let selectedTypeOption = typeOptions.find(o => o.value === type);
        if(!selectedTypeOption) {
            selectedTypeOption = {value: type, label: LocaleProvider(`schema_type_${type}`)}
            if(selectedTypeOption.value === 'array_of_file') {
                const fileTypeOption = typeOptions.find(o => o.value === 'file');
                if(fileTypeOption) {
                    const index = typeOptions.indexOf(fileTypeOption);
                    typeOptions[index] = selectedTypeOption;
                }
                else {
                    typeOptions.push(selectedTypeOption);
                }
            }
            else {
                typeOptions.push(selectedTypeOption);
            }
        }
        return (
            <div className={"schema-item"}>
                {collapseIcon && <div className={"collapse-icon-handler"}>{collapseIcon}</div>}
                <div className={"drag-icon-handler"}>{dragHandler}</div>
                <div className={cn("schema-item-name", {"required-field": this.props.item.required})}>
                    <div className="system-input-container size-s value-m minimal">
                        <FieldInput
                            ref={x => this.fieldInputRef = x}
                            onChange={this.handleChangeName}
                            value={item.key}
                        />
                        {keyInvalidMessage && <Tooltip placement="top" title={keyInvalidMessage}>
                            <i className="far fa-triangle-exclamation reserved-key-alert" />
                        </Tooltip>}
                    </div>
                    {this.props.item.required && <Tooltip placement="top" title={LocaleProvider('field.required-tooltip')}>
                        <span className="field-badge required-badge">R</span>
                    </Tooltip>}
                    {this.props.item.data.foreignKeys && this.props.item.data.foreignKeys.length > 0 && <Tooltip placement="top" title={LocaleProvider('field.has-foreign-key')}>
                        <span className="field-badge">L</span>
                    </Tooltip>}
                    {this.props.item.data.flags && this.props.item.data.flags.includes('unique') && <Tooltip placement="top" title={LocaleProvider('field.is-unique')}>
                        <span className="field-badge">U</span>
                    </Tooltip>}
                    {this.props.item.data.triggers && this.props.item.data.triggers.length > 0 && <Tooltip placement="top" title={LocaleProvider('field.has-triggers')}>
                        <span className="field-badge">T</span>
                    </Tooltip>}
                    {this.props.reservedFieldIds && this.props.reservedFieldIds.includes(this.props.item.id) > 0 && <Tooltip placement="top" title={LocaleProvider('field.has-reserved')}>
                        <span className="field-reserved">
                            <i className="fad fa-circle-dot"/>
                        </span>
                    </Tooltip>}
                </div>
                <div className="entry-type">
                    <div className="system-input-container select size-s value-s minimal">
                        <Select
                            value={selectedTypeOption}
                            onChange={this.handleChangeType}
                            options={typeOptions}
                            placeholder={""}
                            isSearchable={false}
                            className="type-select-style"
                            classNamePrefix="react-select"
                            menuPlacement={"auto"}
                            components={typeItemComponents}
                        />
                    </div>
                </div>
                <div className={"schema-item-actions"}>
                    <div>{this.renderPlusButton()}</div>
                    <div>{this.renderDuplicateButton()}</div>
                    <div>
                        {this.canDeleteItem() &&
                        <button className="system-button size-s minimal color-error icon-button delete-item" onClick={this.deleteItem}>
                            <i className="far fa-trash-can button-icon" />
                        </button>}
                    </div>
                    <div>{this.renderSettingButton()}</div>
                </div>
            </div>
        );
    }

    renderSettingButton() {
        if (this.props.customSettingButton) {
            return (
                <div role="button" className="adv-set" onClick={() => this.props.showAdvanced(this.props.item, this.props.onChange)}>
                    {this.props.customSettingButton}
                </div>
            )
        }
        return (
            <Tooltip placement="top" title={LocaleProvider('adv_setting')}>
                <button type="button" className="system-button size-s minimal icon-button grayscale adv-set" onClick={() => this.props.showAdvanced(this.props.item, this.props.onChange)}>
                    <i className="far fa-gear button-icon" />
                </button>
            </Tooltip>
        )
    }

    hasFieldTypeLocked() {
        return this.props.lockedFieldTypeIds && this.props.lockedFieldTypeIds.indexOf(this.props.item.id) !== -1
    }

    getTypes() {
        if (this.props.item.data.primaryKey) {
            return ['string', 'number'];
        }
        if(this.hasFieldTypeLocked()) {
            const dataType = this.getDataType();
            if(LOCKED_FIELD_TYPE[dataType]) {
                return SCHEMA_TYPE.filter(t => dataType === t || LOCKED_FIELD_TYPE[dataType].indexOf(t) !== -1);
            }
            return [dataType];
        }
        return SCHEMA_TYPE;
    }

    canDeleteItem() {
        if (this.props.parent && this.props.parent.length > 1) {
            if (this.props.item.data.type === 'string' || this.props.item.data.type === 'number' || this.props.item.data.type === 'integer') {
                return !this.props.item.data.primaryKey;
            }
            return true;
        }
        return false;
    }

    hasAnotherSameKey() {
        return this.props.parent && this.props.parent.filter(item => item.key === this.props.item.key).length > 1
    }

    hasReservedMatchedKey() {
        if (this.props.reservedFieldKeyRegex) {
            return !!this.props.reservedFieldKeyRegex.exec(this.props.item.key);
        }
        return false;
    }

    deleteItem() {
        this.props.deleteItem();
    }

    cleanId(item) {
        if(item.hasOwnProperty('id')) {
            delete item.id;
        }
        if (item.data && item.data['$id']) {
            delete item.data['$id'];
        }
        if(item.children) {
            item.children.forEach(childItem => this.cleanId(childItem));
        }
    }

    clone() {
        const clonedItem = JSON.parse(JSON.stringify(this.props.item));
        this.cleanId(clonedItem);
        let fieldNum = 0;
        const baseKey = clonedItem.key;
        let key = baseKey;
        while (this.props.parent.filter(item => item.key === key).length > 0) {
            fieldNum++;
            key = `${baseKey}${fieldNum}`
        }
        clonedItem.key = key;
        this.props.addSiblingNode(clonedItem);
    }

    renderDuplicateButton() {
        if(this.props.item.data.type !== 'object' && this.props.item.data.type !== 'array'){
            return;
        }
        if(this.props.customDuplicateFieldButton) {
            if(typeof(this.props.customDuplicateFieldButton) === 'function') {
                return this.props.customDuplicateFieldButton(this.clone);
            }
            return (
                <Tooltip placement="top" title={LocaleProvider('duplicate_field')}>
                    <div role="button" className="duplicate-item-button" onClick={this.clone}>
                        {this.props.customDuplicateFieldButton}
                    </div>
                </Tooltip>
            )
        }
        return (
            <Tooltip placement="top" title={LocaleProvider('duplicate_field')}>
                <button type="button" className="system-button size-s minimal grayscale icon-button duplicate-item-button" onClick={this.clone}>
                    <i className="far fa-clone button-icon" />
                </button>
            </Tooltip>
        )
    }

    renderPlusButton() {
        if(this.props.customAddFieldButton) {
            if(typeof(this.props.customAddFieldButton) === 'function') {
                return this.props.customAddFieldButton(this.addSiblingNode, this.addChildNode);
            }
            return (
                <div role="button" className="add-item-button" onClick={this.addSiblingNode}>
                    {this.props.customAddFieldButton}
                </div>
            )
        }
        if (this.props.item.data.type !== 'object') {
            return (
                <Tooltip placement="top" title={LocaleProvider('add_sibling_node')}>
                    <button type="button" className="system-button size-s minimal grayscale icon-button add-item-button" onClick={this.addSiblingNode}>
                        <i className="fas fa-plus button-icon" />
                    </button>
                </Tooltip>
            )
        }
        const menu = (
            <Menu>
                <Menu.Item>
                    <span onClick={this.addSiblingNode}>
                      {LocaleProvider('sibling_node')}
                    </span>
                </Menu.Item>
                <Menu.Item>
                    <span onClick={this.addChildNode}>
                        {LocaleProvider('child_node')}
                    </span>
                </Menu.Item>
            </Menu>
        );

        return (
            <Tooltip className="add-item-button" placement="top" title={LocaleProvider('add_node')}>
                <Dropdown overlay={menu}>
                    <button type="button" className="system-button size-s minimal grayscale icon-button add-item-button">
                        <i className="fas fa-plus button-icon" />
                    </button>
                </Dropdown>
            </Tooltip>
        );
    }

    getFlags() {
        return this.props.item.data.flags ? this.props.item.data.flags.split(',').map(f => f.trim()) : [];
    }

    addChildNode() {
        this.props.addChildNode(this.generateNewItem(this.props.item.hasOwnProperty('children') ? this.props.item.children : []));
    }

    addSiblingNode() {
        this.props.addSiblingNode(this.generateNewItem(this.props.parent));
    }

    generateNewItem(parent) {
        let fieldNum = 1;
        let key = 'field_' + fieldNum;
        while (parent.filter(item => item.key === key).length > 0) {
            fieldNum++;
            key = 'field_' + fieldNum;
        }
        return {
            key,
            data: {
                type: 'string',
                isFullText: true
            },
        };
    }

    handleChangeName(event) {
        this.props.item.key = event.target.value.trim();
        this.props.onChange(this.props.item);
    }

    handleEnableRequire(event) {
        this.props.item.required = event.target.checked;
        if (this.props.item.data.primaryKey) {
            this.props.item.required = true;
        }
        this.setFlag('mandatory', this.props.item.required);
        this.forceUpdate();
        this.props.onChange(this.props.item);
    }

    handleFlagEnable(name, event) {
        let enabled = event.target.checked;
        if (this.props.item.data.primaryKey && (name === 'unique' || name === 'mandatory')) {
            enabled = true;
        }
        this.setFlag(name, enabled);
        this.forceUpdate();
        this.props.onChange(this.props.item);
    }

    setFlag(name, enabled) {
        let flags = this.getFlags();
        if (enabled && flags.indexOf(name) === -1) {
            flags.push(name);
        } else if (!enabled) {
            flags = flags.filter(f => f !== name);
        }
        this.props.item.data.flags = flags.join(', ');
        if (this.props.item.data.flags === '') {
            delete this.props.item.data.flags;
        }
    }

    handleChangeType(option) {
        if(!option) {
            return;
        }
        const type = option.value;
        if (this.props.item.data.type === type) {
            return;
        }
        this.props.item.data.type = type;
        if (type !== 'object' || type !== 'array') {
            this.props.item.children = [];
            if(this.props.item.data.items) {
                delete this.props.item.data.items;
            }
            if (type === 'datetime') {
                this.props.item.data.type = 'string';
                this.props.item.data.format = 'date-time';
            } else if (type === 'date') {
                this.props.item.data.type = 'string';
                this.props.item.data.format = 'date';
            } else if (type === 'string' && this.props.item.data.format) {
                this.props.item.data.isFullText = true;
                delete this.props.item.data.format;
            } else if (type === 'file' || type === 'array_of_file' ) {
                this.props.item.data.consumes = [
                    "application/pdf",
                    "image/jpeg",
                    "image/png",
                    "image/gif",
                    "image/tiff",
                    "image/bmp",
                    "image/x-ms-bmp",
                    "application/rtf",
                    "text/rtf",
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                    "application/vnd.oasis.opendocument.text",
                    "application/msword"
                ];
                this.props.item.data.antivirus = true;
                if (type === 'array_of_file') {
                    this.props.item.data.type = 'file';
                    this.props.item.data = {
                        type: 'array',
                        items: this.props.item.data
                    }
                }
            }
            else if(type === 'catalog') {
                this.props.item.data.properties = {
                    key: {
                        type: 'string'
                    },
                    value: {
                        type: 'string'
                    }
                };
                let payload = {
                    key: 'key',
                    data: this.props.item.data.properties.key
                };
                this.props.addChildNode(payload, []);
                this.props.item.data.properties.key['$id'] = payload.id;
                payload = {
                    key: 'value',
                    data: this.props.item.data.properties.value
                };
                this.props.addChildNode(payload, []);
                this.props.item.data.properties.value['$id'] = payload.id;
                this.props.item.data.required = ['key', 'value'];
                this.props.item.children = [];
            }

        }
        if ((type === 'object' || type === 'array') && this.props.item.children.length === 0) {
            this.addChildNode();
        }
        this.forceUpdate();
        this.props.onChange(this.props.item);
    }

    handleChangeDesc(event) {
        this.props.item.data.description = event.target.value;
        this.props.onChange(this.props.item);
    }

    focus() {
        if(this.fieldInputRef) {
            this.fieldInputRef.focus();
        }
    }

}
