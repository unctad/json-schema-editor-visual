import React, {PureComponent} from 'react';
import cn from 'classnames';
import {
    Input,
    InputNumber,
    Row,
    Col,
    Checkbox,
    Icon,
    Tooltip,
    Switch, Tabs
} from 'antd';

const {TextArea} = Input;
import './schemaJson.css';
import _ from 'underscore';
import PropTypes from 'prop-types';


import AceEditor from '../AceEditor/AceEditor.js';
import LocalProvider from '../LocalProvider/index.js';
import MimeTypeInput from "../MimeTypeInput";
import LocaleProvider from "../LocalProvider";
import {textFormat} from "../../utils";
import SchemaTrigger from "./SchemaTrigger";
import MultiSelect from "../MultiSelect";
import ReactSelect from 'react-select';
import SchemaDescription from "./SchemaDescription";

const changeOtherValue = (value, name, data, change) => {
    if (value == null) {
        delete data[name];
    }
    else {
        data[name] = value;
    }
    change(data);
};


class IdValue extends PureComponent {
    constructor(props) {
        super(props);
    }

    changeOtherValue = (value, name, data) => {
        if (value == null) {
            delete data[name];
        }
        else {
            data[name] = value;
        }
        this.props.onChange(data);
    };

    render() {
        const {data, uniqueIdList, maxIdValue} = this.props;
        const idDuplicated = uniqueIdList.filter(id => id === data['$id']).length > 0;
        const isInvalid = maxIdValue < data['$id'];
        return (
            <Col span={8} className={cn(this.props.className, {'has-warning': idDuplicated || isInvalid})}>
                <div className="system-label">
                    {LocalProvider('field.id')}
                </div>
                <div className="system-input-container size-s value-m">
                    <InputNumber
                        value={data['$id']}
                        placeholder={LocalProvider('field.id.placeholder')}
                        onChange={e => this.changeOtherValue(e, '$id', data)}
                        max={maxIdValue}
                        min={1}
                    />
                </div>
                {(idDuplicated || isInvalid) &&
                <span className='duplicated-id-alert'>
                    <Tooltip placement="top"
                                title={textFormat(LocaleProvider(idDuplicated ? 'field.id.already' : 'field.id.invalid'), {maxId: maxIdValue})}>
                        <Icon style={{color: '#ffb818'}} type="warning" className="warning"/>
                    </Tooltip>
                </span>}
            </Col>
        )
    }
}

class FieldConsecutiveIndex extends PureComponent {
    constructor(props) {
        super(props);
    }

    changeOtherValue = (value, name, data) => {
        if (value == null) {
            delete data[name];
        }
        else {
            data[name] = value;
        }
        this.props.onChange(data);
    };

    render() {
        const {data} = this.props;
        return (
            <div className="toggle-field-container">
                <Switch
                    checked={data.consecutiveIndex ? data.consecutiveIndex : false}
                    onChange={e => this.changeOtherValue(e, 'consecutiveIndex', data)}
                />
                <div className="system-label">
                    {LocalProvider('field.consecutive.index')}
                </div>
            </div>
        )
    }
}

class FieldReadOnly extends PureComponent {
    constructor(props) {
        super(props);
    }

    changeOtherValue = (value, name, data) => {
        if (value == null) {
            delete data[name];
        }
        else {
            data[name] = value;
        }
        this.props.onChange(data);
    };

    render() {
        const {data} = this.props;
        return (
            <div className={cn("toggle-field-container", this.props.className)}>
                <Switch
                    checked={data.readOnly ? data.readOnly : false}
                    onChange={e => this.changeOtherValue(e, 'readOnly', data)}
                />
                <div className="system-label">
                    {LocalProvider('field.readonly')}
                </div>
            </div>
        )
    }
}

class FieldTrimText extends PureComponent {
    constructor(props) {
        super(props);
    }

    changeOtherValue = (value, name, data) => {
        if (value == null) {
            delete data[name];
        }
        else {
            data[name] = value;
        }
        this.props.onChange(data);
    };

    render() {
        const {data} = this.props;
        return (
            <div className="toggle-field-container label-space">
                <Switch
                    checked={(data.trimText === undefined || data.trimText === null) ? true : data.trimText}
                    onChange={e => this.changeOtherValue(e, 'trimText', data)}
                />
                <div className="system-label">
                    {LocalProvider('field.trim_text')}
                </div>
            </div>
        )
    }
}

class FlagCheckbox extends PureComponent {
    constructor(props) {
        super(props);
    }

    changeOtherValue = (value, data) => {
        let flags = data.flags ? data.flags.split(',').map(f => f.trim()) : [];

        if(value) {
            flags.push(this.props.name);
        }
        else {
            flags = flags.filter(f => f !== this.props.name);
        }
        data.flags = flags.join(',');
        this.props.onChange(data);
    };

    render() {
        const {data} = this.props;
        const flags = data.flags ? data.flags.split(',').map(f => f.trim()) : [];
        return (
            <div className={cn("toggle-field-container", this.props.className)}>
                <Switch
                    checked={flags.indexOf(this.props.name) !== -1}
                    onChange={e => this.changeOtherValue(e, data)}
                />
                <div className="system-label">
                    {LocalProvider(`flag.item-${this.props.name}`)}
                </div>
            </div>
        )
    }
}

class FieldOrderDirection extends PureComponent {
    constructor(props) {
        super(props);
    }

    changeOtherValue = (value, name, data) => {
        if (value == null) {
            delete data[name];
        }
        else {
            data[name] = value;
        }
        this.props.onChange(data);
    };

    render() {
        const {data} = this.props;
        return (
            <Row className="other-row" type="flex">
                <Col span={12} className="toggle-field-container">
                    <Switch
                        checked={data.sortable}
                        onChange={v => this.changeOtherValue(v, 'sortable', data)}
                    />
                    <div className="system-label">
                        {LocalProvider('field.sortable')}
                    </div>
                </Col>
                <Col span={12} className="toggle-field-container">
                    <Switch
                        disabled={!data.sortable}
                        checked={!data.orderDescending}
                        onChange={v => this.changeOtherValue(!v, 'orderDescending', data)}
                    />
                    <div className="system-label">
                        {LocalProvider('field.order_ascending')}
                    </div>
                </Col>
            </Row>
        )
    }
}

class ForeignKey extends PureComponent {
    constructor(props) {
        super(props);
        this.onChangeCheckBox = this.onChangeCheckBox.bind(this);
        this.onChangeForeignDatabase = this.onChangeForeignDatabase.bind(this);
        this.onChangeSelect = this.onChangeSelect.bind(this);
        this.add = this.add.bind(this);
    }

    changeOtherValue = (value, name) => {
        let data = this.props.data;
        if (value == null) {
            delete data[name];
        }
        else {
            data[name] = value;
        }
        this.props.onChange(data);
    };

    getFlags() {
        if (this.props.data.flags) {
            return this.props.data.flags.split(",").map(f => f.trim());
        }
        return [];
    }

    getForeignDatabase() {
        const f = this.getFlags().filter(f => f.startsWith('foreign-key'));
        if (f.length === 0) {
            return null;
        }
        const db = f[0].substring('foreign-key'.length);
        if (db.length === 0) {
            return db;
        }
        const part = db.substring(1).split('-');

        return {
            key: part.shift(),
            fieldKeys: part
        };
    }

    onChangeCheckBox(value) {
        let {data} = this.props;
        if (value) {
            data.foreignKeys = [{
                databaseKey: '',
                values: []
            }];
        } else {
            delete data.foreignKeys;
        }
        this.props.onChange(data);
    }

    getSelectedOptions(foreignKey, selectedDatabase) {
        let selectedOptions = [];
        if (!foreignKey || !selectedDatabase) {
            return selectedOptions;
        }
        foreignKey.values.forEach(value => {
            if(typeof (value) === 'object') {
                let field = selectedDatabase.fields.filter(field => field.key === value.fieldKey);
                if (field.length > 0) {
                    selectedOptions.push({value: value.fieldKey, label: field[0].name, type: 'field'});
                } else {
                    selectedOptions.push({value: value.fieldKey, label: value.fieldKey, type: 'field'});
                }
            }
            else {
                selectedOptions.push({value: String(value), label: String(value)})
            }
        });
        return selectedOptions;
    }

    getForeignKeys() {
        if (this.props.data.foreignKeys) {
            return this.props.data.foreignKeys;
        }
        const foreignDatabase = this.getForeignDatabase();
        if (foreignDatabase) {
            const values = [];
            const foreignFieldsBetween = this.props.data.foreignFieldsBetween || [];
            foreignDatabase.fieldKeys.forEach((fieldKey, fieldKeyIndex) => {
                values.push({
                    fieldKey
                });
                if (foreignFieldsBetween.length > fieldKeyIndex && foreignFieldsBetween[fieldKeyIndex].length > 0) {
                    values.push(foreignFieldsBetween[fieldKeyIndex]);
                }
            });
            let {data} = this.props;
            if (data.foreignFieldsBetween !== undefined) {
                delete data.foreignFieldsBetween;
            }
            data.flags = this.getFlags().filter(f => !f.startsWith('foreign-key')).join(', ');
            data.foreignKeys = [{
                databaseKey: foreignDatabase.key,
                values
            }];
            setTimeout(this.props.onChange.bind(this, data), 100);
            return data.foreignKeys;
        }
        return null;
    }

    render() {
        const databaseOptions = [];
        const databaseOptionItems = [];
        if(this.props.databases) {
            for(const database of this.props.databases) {
                const itemOption = {label: database.name, value: database.key};
                databaseOptionItems.push(itemOption);
                if(database.groupName) {
                    let groupOption = databaseOptions.find(o => o.label === database.groupName && o.options != null);
                    if(!groupOption) {
                        groupOption = {
                            label: database.groupName,
                            options: []
                        }
                        databaseOptions.push(groupOption);
                    }
                    groupOption.options.push(itemOption)
                }
                else {
                    databaseOptions.push(itemOption);
                }
            }
        }
        const foreignKeys = this.getForeignKeys();
        return (
            <div className='fk-settings-container'>
                <Row className="fk-settings" type="flex" align="middle">
                    <Col span={8} className="toggle-field-container">
                        <Switch
                            checked={foreignKeys && foreignKeys.length > 0}
                            onChange={e => this.onChangeCheckBox(e)}
                        />
                        <div className="system-label">
                            {LocalProvider('foreign_key')}
                        </div>
                    </Col>
                    <Col span={24} className='fk-fields-list'>
                        {foreignKeys && foreignKeys.length > 0 && foreignKeys.map((foreignKey, foreignKeyIndex) => {
                            const selectedDatabases = foreignKey.databaseKey ? this.props.databases.filter(d => d.key === foreignKey.databaseKey) : null;
                            const selectedDatabase = selectedDatabases && selectedDatabases.length > 0 ? selectedDatabases[0] : null;
                            const selectedDatabaseOption = foreignKey.databaseKey ? databaseOptionItems.find(o => o.value === foreignKey.databaseKey) : null;
                            return (
                                <Row key={foreignKeyIndex} type="flex" className="fk-list">
                                    <Col span={8}>
                                        <div className="system-input-container select size-s value-m">
                                            <ReactSelect
                                                value={selectedDatabaseOption}
                                                isClearable={true}
                                                placeholder={""}
                                                options={databaseOptions}
                                                onChange={this.onChangeForeignDatabase.bind(this, foreignKey)}
                                                className="type-select-style"
                                                classNamePrefix="react-select"
                                            />
                                        </div>
                                    </Col>
                                    <Col span={11}>
                                        <div className="system-input-container select multiple size-s value-m">
                                            <MultiSelect
                                                selectedOption={this.getSelectedOptions(foreignKey, selectedDatabase)}
                                                options={selectedDatabase ? selectedDatabase.fields.map(field => ({
                                                    value: field.key,
                                                    label: field.name,
                                                    type: 'field'
                                                })) : []}
                                                onChange={this.onChangeSelect.bind(this, foreignKey)}
                                                placeholder={""}
                                                disabled={!selectedDatabase}
                                                className="type-select-style"
                                                classNamePrefix="react-select"
                                            />
                                        </div>
                                    </Col>
                                    <Col span={3} className={"fk-item-checkbox"}>
                                        <Tooltip title={LocalProvider('foreign_key.delete_cascade')}>
                                            <span>D</span><Checkbox checked={!!foreignKey.deleteCascade} onChange={this.onChangeCheckBoxDeleteCascade.bind(this, foreignKey)}/>
                                        </Tooltip>
                                        <Tooltip title={LocalProvider('foreign_key.update_cascade')}>
                                            <span>U</span><Checkbox checked={!!foreignKey.updateCascade} onChange={this.onChangeCheckBoxUpdateCascade.bind(this, foreignKey)}/>
                                        </Tooltip>
                                    </Col>
                                    <Col span={2} className={"fk-item-action"}>
                                        <button type="button" className="system-button size-s minimal grayscale icon-button" onClick={this.remove.bind(this, foreignKeyIndex)}>
                                            <i className="fas fa-minus button-icon" />
                                        </button>
                                        <button type="button" className="system-button size-s minimal grayscale icon-button" onClick={this.add}>
                                            <i className="fas fa-plus button-icon" />
                                        </button>
                                    </Col>
                                </Row>
                            )
                        })}
                    </Col>
                </Row>
            </div>
        )
    }

    onChangeCheckBoxUpdateCascade(foreignKey, e) {
        foreignKey.updateCascade = e.target.checked;
        this.props.onChange(this.props.data);
    }

    onChangeCheckBoxDeleteCascade(foreignKey, e) {
        foreignKey.deleteCascade = e.target.checked;
        this.props.onChange(this.props.data);
    }

    remove(index) {
        let {data} = this.props;
        data.foreignKeys.splice(index, 1);
        if(data.foreignKeys.length === 0) {
            delete data.foreignKeys;
        }
        this.props.onChange(this.props.data);
    }

    add() {
        let {data} = this.props;
        data.foreignKeys.push({
            databaseKey: '',
            values: []
        });
        this.props.onChange(this.props.data);
    }

    onChangeSelect(foreignKey, selected) {
        let values = [];
        for(const selectedOption of selected) {
            const value = selectedOption.type === 'field' ? {fieldKey: selectedOption.value} : selectedOption.value;
            if(typeof(value) === 'string') {
                const beforeValue = values.length > 0 ? values[values.length - 1] : null;
                if(typeof(beforeValue) === 'string') {
                    values[values.length - 1] += value;
                    continue;
                }
            }
            values.push(value);
        }
        foreignKey.values = values;
        this.props.onChange(this.props.data);
    }

    onChangeForeignDatabase(foreignKey, selectedOption) {
        foreignKey.databaseKey = selectedOption ? selectedOption.value : '';
        foreignKey.values = [];
        this.props.onChange(this.props.data);
    }

}

class SchemaString extends PureComponent {
    constructor(props, context) {
        super(props);
        this.state = {
            checked: !_.isUndefined(props.data.enum)
        };
        this.format = context.Model.__jsonSchemaFormat;
    }

    UNSAFE_componentWillReceiveProps(nextprops) {
        if (this.props.data.enum !== nextprops.data.enum) {
            this.setState({
                checked: !_.isUndefined(nextprops.data.enum)
            });
        }
    }

    changeOtherValue = (value, name, data) => {
        if (value == null) {
            delete data[name];
        }
        else {
            data[name] = value;
        }
        this.props.onChange(data);
    };

    changeEnumOtherValue = (value, data) => {
        var arr = value.split('\n');
        if (arr.length === 0 || (arr.length === 1 && !arr[0])) {
            delete data.enum;
            this.props.onChange(data);
        } else {
            data.enum = arr;
            this.props.onChange(data);
        }
    };

    changeEnumDescOtherValue = (value, data) => {
        data.enumDesc = value;
        this.props.onChange(data);
    };

    onChangeCheckBox = (checked, data) => {
        this.setState({
            checked
        });
        if (!checked) {
            delete data.enum;
            this.props.onChange(data);
        }
    };

    render() {
        const {data, uniqueIdList, maxIdValue, showAllAdvanced} = this.props;
        return (
            <div className="advanced-settings-rows">
                {showAllAdvanced && <Row className="other-row space-below" type="flex" align="middle">
                    <IdValue data={data} onChange={this.props.onChange} uniqueIdList={uniqueIdList}
                                             maxIdValue={maxIdValue} className="advanced-property" />
                </Row>}
                <Row className="other-row" type="flex" align="middle">
                    <Col span={8}>
                        <FlagCheckbox data={data} onChange={this.props.onChange} name={"unique"} className="inverted" />
                    </Col>
                    <Col span={8}>
                        <FlagCheckbox data={data} onChange={this.props.onChange} name={"mandatory"} className="inverted" />
                    </Col>
                    {showAllAdvanced && <Col span={8}>
                        <FieldReadOnly data={data} onChange={this.props.onChange} className="inverted"/>
                    </Col>}
                </Row>

                {showAllAdvanced && <Row className="other-row" type="flex">
                    {/*<Col span={6} className="system-label left">*/}
                    {/*    <span>{LocalProvider('format')} :</span>*/}
                    {/*    <Select*/}
                    {/*        showSearch*/}
                    {/*        className="full"*/}
                    {/*        value={data.format}*/}
                    {/*        dropdownClassName="json-schema-react-editor-adv-modal-select"*/}
                    {/*        placeholder={LocalProvider('select-format')}*/}
                    {/*        optionFilterProp="children"*/}
                    {/*        optionLabelProp="value"*/}
                    {/*        onChange={e => this.changeOtherValue(e, 'format', data)}*/}
                    {/*        filterOption={(input, option) => {*/}
                    {/*            return option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0;*/}
                    {/*        }}*/}
                    {/*    >*/}
                    {/*        {this.format.map(item => {*/}
                    {/*            return (*/}
                    {/*                <Select.Option value={item.name} key={item.name}>*/}
                    {/*                    {item.name} <span className="format-items-title">{item.title}</span>*/}
                    {/*                </Select.Option>*/}
                    {/*            );*/}
                    {/*        })}*/}
                    {/*    </Select>*/}
                    {/*</Col>*/}
                    <Col span={8}>
                        <div className="system-label">
                            {LocalProvider('minLength')}
                        </div>
                        <div className="system-input-container size-s value-m">
                            <InputNumber
                                value={data.minLength}
                                placeholder="min.length"
                                onChange={e => this.changeOtherValue(e, 'minLength', data)}
                                className="full"
                            />
                        </div>
                    </Col>
                    <Col span={8}>
                        <div className="system-label">
                            {LocalProvider('maxLength')}
                        </div>
                        <div className="system-input-container size-s value-m">
                            <InputNumber
                                value={data.maxLength}
                                placeholder="max.length"
                                onChange={e => this.changeOtherValue(e, 'maxLength', data)}
                                className="full"
                            />
                        </div>
                    </Col>
                    <Col span={8}>
                        <FieldTrimText data={data} onChange={this.props.onChange}/>
                    </Col>
                </Row>}

                <Row className="other-row space-below" type="flex">
                    <Col span={8}>
                        <div className="system-label">
                            {LocalProvider('default')}
                        </div>
                        <div className="system-input-container size-s value-m">
                            <Input
                                value={data.default}
                                placeholder={LocalProvider('default')}
                                onChange={e => this.changeOtherValue(e.target.value, 'default', data)}
                            />
                        </div>
                    </Col>
                    <Col span={8}>
                        <div className="system-label">
                            {LocalProvider('pattern-label')}
                            <Tooltip title={LocalProvider('pattern')}>
                                <i className="far fa-circle-question help-icon" />
                            </Tooltip>
                        </div>
                        <div className="system-input-container size-s value-m">
                            <Input
                                value={data.pattern}
                                placeholder="Pattern"
                                onChange={e => this.changeOtherValue(e.target.value, 'pattern', data)}
                            />
                        </div>
                    </Col>
                    <Col span={8}>
                        <div className="system-label">
                            {LocalProvider('enum')}
                        </div>
                        <div className="system-input-container textarea size-s value-m">
                            <TextArea
                                value={data.enum && data.enum.length && data.enum.join('\n')}
                                placeholder={LocalProvider('enum_msg')}
                                autoSize={{minRows: 2, maxRows: 6}}
                                onChange={e => {
                                    this.changeEnumOtherValue(e.target.value, data);
                                }}
                            />
                        </div>
                    </Col>
                </Row>
                {showAllAdvanced && <Row className="other-row" type="flex" align="middle">
                    <Col span={8}>
                        <FieldConsecutiveIndex data={data} onChange={this.props.onChange}/>
                    </Col>
                    <Col span={16}>
                        <FlagCheckbox data={data} onChange={this.props.onChange} name={"listing-value"}
                                      className="vertical"/>
                    </Col>
                </Row>}
                <Row className="other-row" type="flex" align="middle">
                    <Col span={8} className="advanced-property toggle-field-container">
                        <Switch
                            checked={data.isFullText}
                            onChange={e => this.changeOtherValue(e, 'isFullText', data)}
                        />
                        <div className="system-label">
                            {LocalProvider('full_text_label')}
                            <Tooltip title={LocalProvider('full_text_help')}>
                                <i className="far fa-circle-question help-icon" />
                            </Tooltip>
                        </div>
                    </Col>
                    <Col span={16}>
                        <FieldOrderDirection data={data} onChange={this.props.onChange} />
                    </Col>
                </Row>

                {/*<Row className="other-row space-below" type="flex" align="top">*/}
                {/*    <Col span={6} className="system-label left">*/}
                {/*        <Row className="gap-10" type="flex" align="middle">*/}
                {/*            <Col className="d-flex">*/}
                {/*                <Switch*/}
                {/*                    checked={this.state.checked}*/}
                {/*                    onChange={e => this.onChangeCheckBox(e, data)}*/}
                {/*                />*/}
                {/*            </Col>*/}
                {/*            <Col>*/}
                {/*                {LocalProvider('enum')}*/}
                {/*            </Col>*/}
                {/*        </Row>*/}
                {/*    </Col>*/}
                {/*    <Col span={9} className="system-label left">*/}
                {/*        <TextArea*/}
                {/*            value={data.enum && data.enum.length && data.enum.join('\n')}*/}
                {/*            disabled={!this.state.checked}*/}
                {/*            placeholder={LocalProvider('enum_msg')}*/}
                {/*            autoSize={{minRows: 2, maxRows: 6}}*/}
                {/*            onChange={e => {*/}
                {/*                this.changeEnumOtherValue(e.target.value, data);*/}
                {/*            }}*/}
                {/*        />*/}
                {/*    </Col>*/}
                {/*    <Col span={9} className="left">*/}
                {/*        {this.state.checked && (*/}
                {/*            <div>*/}
                {/*                <TextArea*/}
                {/*                    value={data.enumDesc}*/}
                {/*                    disabled={!this.state.checked}*/}
                {/*                    placeholder={LocalProvider('enum_desc_msg')}*/}
                {/*                    autoSize={{minRows: 2, maxRows: 6}}*/}
                {/*                    onChange={e => {*/}
                {/*                        this.changeEnumDescOtherValue(e.target.value, data);*/}
                {/*                    }}*/}
                {/*                />*/}
                {/*            </div>*/}
                {/*        )}*/}
                {/*    </Col>*/}
                {/*</Row>*/}

                {showAllAdvanced && <Row className={cn("other-row", "gap-10", {'space-below': this.props.databases})} type="flex"
                      align="middle">
                    <Col span={8}>
                        <div className="system-label">
                            {LocalProvider('tags_label')}
                            <Tooltip title={LocalProvider('tags_help')}>
                                <i className="far fa-circle-question help-icon" />
                            </Tooltip>
                        </div>
                        <div className="system-input-container size-s value-m">
                            <Input
                                value={data.tags}
                                placeholder={LocalProvider('tags_placeholder')}
                                onChange={e => this.changeOtherValue(e.target.value, 'tags', data)}
                            />
                        </div>
                    </Col>
                </Row>}

                {this.props.databases &&
                <ForeignKey data={data} onChange={this.props.onChange} databases={this.props.databases}/>}

                {/*<Row className="other-row hide" type="flex" align="middle">*/}
                {/*    <Col span={4} className="system-label">*/}
                {/*        <span>*/}
                {/*            {LocalProvider('flags_label')}*/}
                {/*            <Tooltip title={LocalProvider('flags_help')}>*/}
                {/*                <Icon type="question-circle-o" style={{width: '10px', marginLeft: '4px'}}/>*/}
                {/*            </Tooltip>*/}
                {/*        </span>*/}
                {/*    </Col>*/}
                {/*    <Col span={20}>*/}
                {/*        <Input*/}
                {/*            value={data.flags}*/}
                {/*            placeholder={LocalProvider('flags_placeholder')}*/}
                {/*            onChange={e => this.changeOtherValue(e.target.value, 'flags', data)}*/}
                {/*        />*/}
                {/*    </Col>*/}
                {/*</Row>*/}
            </div>
        );
    }
}

SchemaString.contextTypes = {
    changeCustomValue: PropTypes.func,
    Model: PropTypes.object
};

class SchemaNumber extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            checked: !_.isUndefined(props.data.enum),
            enum: _.isUndefined(props.data.enum) ? '' : props.data.enum.join('\n')
        };
    }

    UNSAFE_componentWillReceiveProps(nextprops) {
        const enumStr = _.isUndefined(this.props.data.enum) ? '' : this.props.data.enum.join('\n');
        const nextEnumStr = _.isUndefined(nextprops.data.enum) ? '' : nextprops.data.enum.join('\n');
        if (enumStr !== nextEnumStr) {
            this.setState({enum: nextEnumStr});
        }
    }

    onChangeCheckBox = (checked, data) => {
        this.setState({
            checked
        });

        if (!checked) {
            delete data.enum;
            this.setState({enum: ''});
            this.props.onChange(data);
        }
    };

    changeEnumOtherValue = (value, data) => {
        this.setState({enum: value});
        let arr = value.split('\n');
        const enumLen = this.state.enum.split('\n').length;
        // 判断是否是删除操作
        if (enumLen > arr.length) {
            data.enum = arr.map(item => +item);
            this.props.onChange(data);
        }
        if (arr.length === 0 || (arr.length == 1 && !arr[0])) {
            delete data.enum;
            this.props.onChange(data);
        }
    };

    onEnterEnumOtherValue = (value, data) => {
        let arr = value.split('\n').map(item => +item);
        data.enum = arr;
        this.props.onChange(data);
    };

    changeEnumDescOtherValue = (value, data) => {
        data.enumDesc = value;
        this.props.onChange(data);
    };

    render() {
        const {data, uniqueIdList, maxIdValue, showAllAdvanced} = this.props;
        return (
            <div>
                {showAllAdvanced && <Row className="other-row space-below" type="flex" align="middle">
                    <IdValue data={data} onChange={this.props.onChange} uniqueIdList={uniqueIdList}
                             maxIdValue={maxIdValue} className="advanced-property" />
                </Row>}
                <Row className="other-row" type="flex" align="middle">
                    <Col span={6}>
                        <FlagCheckbox data={data} onChange={this.props.onChange} name={"unique"} className="inverted" />
                    </Col>
                    <Col span={6}>
                        <FlagCheckbox data={data} onChange={this.props.onChange} name={"mandatory"} className="inverted" />
                    </Col>
                    {showAllAdvanced && <Col span={6}>
                        <FieldReadOnly data={data} onChange={this.props.onChange} className="inverted"/>
                    </Col>}
                </Row>

                <Row className="other-row space-below" type="flex" align="middle">
                    <Col span={12}>
                        <div className="system-label">
                            {LocalProvider('default')}
                        </div>
                        <div className="system-input-container size-s value-m">
                            <Input
                                value={data.default}
                                placeholder={LocalProvider('default')}
                                onChange={e =>
                                    changeOtherValue(e.target.value, 'default', data, this.props.onChange)
                                }
                            />
                        </div>
                    </Col>
                    <Col span={12}>
                        <div className="system-label">
                            {LocalProvider('enum')}
                        </div>
                        <div className="system-input-container textarea size-s value-m">
                            <TextArea
                                // value={data.enum && data.enum.length && data.enum.join('\n')}
                                value={this.state.enum}
                                placeholder={LocalProvider('enum_msg')}
                                autoSize={{minRows: 2, maxRows: 6}}
                                onChange={e => {
                                    this.changeEnumOtherValue(e.target.value, data);
                                }}
                                onPressEnter={e => this.onEnterEnumOtherValue(e.target.value, data)}
                            />
                        </div>
                    </Col>
                </Row>

                {showAllAdvanced && <Row className="other-row space-below" type="flex" align="middle">
                    <Col span={4}>
                        <FieldConsecutiveIndex data={data} onChange={this.props.onChange}/>
                    </Col>
                    <Col span={4}>
                        <FlagCheckbox data={data} onChange={this.props.onChange} name={"listing-value"}
                                      className="vertical"/>
                    </Col>
                    <Col span={4}>
                        <div>
                            <div className="input-like-height">
                                <Switch
                                    checked={data['x-use-thousands-separator'] == null ? true : data['x-use-thousands-separator']}
                                    onChange={e =>
                                        changeOtherValue(e, 'x-use-thousands-separator', data, this.props.onChange)
                                    }
                                />
                            </div>
                            <div className="system-label">
                                {LocalProvider('field.number.use-thousands-separator')}
                            </div>
                        </div>
                    </Col>
                </Row>}

                <Row className="other-row" type="flex" align="middle">
                    {showAllAdvanced && <Col span={8} className="advanced-property toggle-field-container">
                        <Switch
                            checked={data.exclusiveMinimum}
                            placeholder="exclusiveMinimum"
                            onChange={e =>
                                changeOtherValue(e, 'exclusiveMinimum', data, this.props.onChange)
                            }
                        />
                        <div className='system-label'>
                            {LocalProvider('exclusiveMinimum')}
                            <Tooltip title={LocalProvider('exclusiveMinimum')}>
                                <i className="far fa-circle-question help-icon" />
                            </Tooltip>
                        </div>
                    </Col>}
                    {showAllAdvanced && <Col span={8} className="advanced-property toggle-field-container">
                        <Switch
                            checked={data.exclusiveMaximum}
                            placeholder="exclusiveMaximum"
                            onChange={e =>
                                changeOtherValue(e, 'exclusiveMaximum', data, this.props.onChange)
                            }
                        />
                        <div className='system-label'>
                            {LocalProvider('exclusiveMaximum')}
                            <Tooltip title={LocalProvider('exclusiveMaximum')}>
                                <i className="far fa-circle-question help-icon" />
                            </Tooltip>
                        </div>
                    </Col>}
                </Row>
                <Row className="other-row" type="flex" align="middle">
                    <Col span={8}>
                        <div className="system-label">
                            {LocalProvider('minimum')}
                        </div>
                        <div className="system-input-container size-s value-m">
                            <InputNumber
                                value={data.minimum}
                                placeholder={LocalProvider('minimum')}
                                onChange={e =>
                                    changeOtherValue(e, 'minimum', data, this.props.onChange)
                                }
                            />
                        </div>
                    </Col>
                    <Col span={8}>
                        <div className="system-label">
                            {LocalProvider('maximum')}
                        </div>
                        <div className="system-input-container size-s value-m">
                            <InputNumber
                                value={data.maximum}
                                placeholder={LocalProvider('maximum')}
                                onChange={e =>
                                    changeOtherValue(e, 'maximum', data, this.props.onChange)
                                }
                            />
                        </div>
                    </Col>
                    <Col span={8} className="toggle-field-container label-space">
                        <Switch
                            checked={data.type === 'number'}
                            onChange={e => changeOtherValue(e ? 'number' : 'integer', 'type', data, this.props.onChange)}
                        />
                        <div className="system-label">
                            {LocalProvider('number_decimal_enable')}
                        </div>
                    </Col>
                </Row>

                <Row className="other-row" type="flex" align="middle">
                    {showAllAdvanced && <Col span={8} className="advanced-property toggle-field-container">
                        <Switch
                            checked={data.isFullText}
                            onChange={e => changeOtherValue(e, 'isFullText', data, this.props.onChange)}
                        />
                        <div className='system-label'>
                            {LocalProvider('full_text_label')}
                            <Tooltip title={LocalProvider('full_text_help')}>
                                <i className="far fa-circle-question help-icon" />
                            </Tooltip>
                        </div>
                    </Col>}
                    <Col span={16}>
                        <FieldOrderDirection data={data} onChange={this.props.onChange} />
                    </Col>
                </Row>

                {/*<Row className="other-row space-below" type="flex" align="top">*/}
                {/*    <Col span={6} className="system-label left">*/}
                {/*        <Row className="gap-10" type="flex" align="middle">*/}
                {/*            <Col className="d-flex">*/}
                {/*                <Switch*/}
                {/*                    checked={this.state.checked}*/}
                {/*                    onChange={e => this.onChangeCheckBox(e, data)}*/}
                {/*                />*/}
                {/*            </Col>*/}
                {/*            <Col>*/}
                {/*                {LocalProvider('enum')}*/}
                {/*            </Col>*/}
                {/*        </Row>*/}
                {/*    </Col>*/}
                {/*    <Col span={9} className="system-label left">*/}
                {/*        <TextArea*/}
                {/*            // value={data.enum && data.enum.length && data.enum.join('\n')}*/}
                {/*            value={this.state.enum}*/}
                {/*            disabled={!this.state.checked}*/}
                {/*            placeholder={LocalProvider('enum_msg')}*/}
                {/*            autoSize={{minRows: 2, maxRows: 6}}*/}
                {/*            onChange={e => {*/}
                {/*                this.changeEnumOtherValue(e.target.value, data);*/}
                {/*            }}*/}
                {/*            onPressEnter={e => this.onEnterEnumOtherValue(e.target.value, data)}*/}
                {/*        />*/}
                {/*    </Col>*/}
                {/*    <Col span={9} className="system-label left">*/}
                {/*        {this.state.checked && (*/}
                {/*            <div>*/}
                {/*                <TextArea*/}
                {/*                    value={data.enumDesc}*/}
                {/*                    disabled={!this.state.checked}*/}
                {/*                    placeholder={LocalProvider('enum_desc_msg')}*/}
                {/*                    autoSize={{minRows: 2, maxRows: 6}}*/}
                {/*                    onChange={e => {*/}
                {/*                        this.changeEnumDescOtherValue(e.target.value, data);*/}
                {/*                    }}*/}
                {/*                />*/}
                {/*            </div>*/}
                {/*        )}*/}
                {/*    </Col>*/}
                {/*</Row>*/}

                {showAllAdvanced && <Row className={cn("other-row", "gap-10", {'space-below': this.props.databases})} type="flex"
                      align="middle">
                    <Col span={12}>
                        <div className="system-label">
                            {LocalProvider('tags_label')}
                            <Tooltip title={LocalProvider('tags_help')}>
                                <i className="far fa-circle-question help-icon" />
                            </Tooltip>
                        </div>
                        <div className="system-input-container size-s value-m">
                            <Input
                                value={data.tags}
                                placeholder={LocalProvider('tags_placeholder')}
                                onChange={e => changeOtherValue(e.target.value, 'tags', data, this.props.onChange)}
                            />
                        </div>
                    </Col>
                </Row>}

                {this.props.databases &&
                <ForeignKey data={data} onChange={this.props.onChange} databases={this.props.databases}/>}

                {/*<Row className="other-row hide" type="flex" align="middle">*/}
                {/*    <Col span={4} className="system-label">*/}
                {/*        <span>*/}
                {/*            {LocalProvider('flags_label')}*/}
                {/*            <Tooltip title={LocalProvider('flags_help')}>*/}
                {/*                <Icon type="question-circle-o" style={{width: '10px', marginLeft: '4px'}}/>*/}
                {/*            </Tooltip>*/}
                {/*        </span>*/}
                {/*    </Col>*/}
                {/*    <Col span={20}>*/}
                {/*        <Input*/}
                {/*            value={data.flags}*/}
                {/*            placeholder={LocalProvider('flags_placeholder')}*/}
                {/*            onChange={e => changeOtherValue(*/}
                {/*                e.target.value,*/}
                {/*                'flags',*/}
                {/*                data,*/}
                {/*                this.props.onChange*/}
                {/*            )}*/}
                {/*        />*/}
                {/*    </Col>*/}
                {/*</Row>*/}
            </div>
        );
    }
}

SchemaNumber.contextTypes = {
    changeCustomValue: PropTypes.func
};

const SchemaBoolean = (props, context) => {
    const {data, onChange, uniqueIdList, maxIdValue, showAllAdvanced} = props;
    const booleanOptions = [
        {label: 'true', value: 'true'},
        {label: 'false', value: 'false'}
    ];
    let value = _.isUndefined(data.default) ? null : data.default ? booleanOptions[0] : booleanOptions[1];
    return (
        <div>
            {showAllAdvanced && <Row className="other-row space-below" type="flex" align="middle">
                <IdValue data={data} onChange={onChange} uniqueIdList={uniqueIdList}
                         maxIdValue={maxIdValue} className="advanced-property"/>
            </Row>}
            <Row className="other-row" type="flex" align="middle">
                <Col span={6}>
                    <FlagCheckbox data={data} onChange={onChange} name={"unique"} className="inverted" />
                </Col>
                <Col span={6}>
                    <FlagCheckbox data={data} onChange={onChange} name={"mandatory"} className="inverted" />
                </Col>
                {showAllAdvanced && <Col span={6}>
                    <FieldReadOnly data={data} onChange={onChange} className="inverted"/>
                </Col>}
            </Row>

            <Row className="other-row space-below" type="flex" align="middle">
                <Col span={6}>
                    <div className="system-label">
                        {LocalProvider('default')}
                    </div>
                    <ReactSelect
                        value={value}
                        isClearable={true}
                        placeholder={""}
                        options={booleanOptions}
                        onChange={selectedOption => {
                            changeOtherValue(
                                selectedOption == null ? undefined : selectedOption.value === 'true',
                                'default',
                                data,
                                onChange
                            )
                        }}
                    />
                </Col>
            </Row>
            {showAllAdvanced && <Row className="other-row">
                <Col span={6}>
                    <FlagCheckbox data={data} onChange={onChange} name={"listing-value"} className="vertical"/>
                </Col>
                <Col span={12}>
                    <FieldOrderDirection data={data} onChange={onChange} vertical/>
                </Col>
            </Row>}

            {showAllAdvanced && <Row className="other-row space-below" type="flex" align="middle">
                <Col span={12} className="advanced-property toggle-field-container">
                    <Switch
                        checked={data.isFullText}
                        onChange={e => changeOtherValue(e, 'isFullText', data, onChange)}
                    />
                    <div className="system-label">
                        {LocalProvider('full_text_label')}
                        <Tooltip title={LocalProvider('full_text_help')}>
                            <i className="far fa-circle-question help-icon" />
                        </Tooltip>
                    </div>
                </Col>
            </Row>}

            {showAllAdvanced && <Row className="other-row gap-10" type="flex" align="middle">
                <Col span={8}>
                    <div className="system-label">
                        {LocalProvider('tags_label')}
                        <Tooltip title={LocalProvider('tags_help')}>
                            <i className="far fa-circle-question help-icon" />
                        </Tooltip>
                    </div>
                    <div className="system-input-container size-s value-m">
                        <Input
                            value={data.tags}
                            placeholder={LocalProvider('tags_placeholder')}
                            onChange={e => changeOtherValue(e.target.value, 'tags', data, onChange)}
                        />
                    </div>
                </Col>
            </Row>}

            {/*<Row className="other-row hide" type="flex" align="middle">*/}
            {/*    <Col span={4} className="system-label">*/}
            {/*        <span>*/}
            {/*            {LocalProvider('flags_label')}*/}
            {/*            <Tooltip title={LocalProvider('flags_help')}>*/}
            {/*                <Icon type="question-circle-o" style={{width: '10px', marginLeft: '4px'}}/>*/}
            {/*            </Tooltip>*/}
            {/*        </span>*/}
            {/*    </Col>*/}
            {/*    <Col span={20}>*/}
            {/*        <Input*/}
            {/*            value={data.flags}*/}
            {/*            placeholder={LocalProvider('flags_placeholder')}*/}
            {/*            onChange={e => changeOtherValue(*/}
            {/*                e.target.value,*/}
            {/*                'flags',*/}
            {/*                data,*/}
            {/*                onChange*/}
            {/*            )}*/}
            {/*        />*/}
            {/*    </Col>*/}
            {/*</Row>*/}
        </div>
    );
};

SchemaBoolean.contextTypes = {
    changeCustomValue: PropTypes.func
};

const SchemaArray = (props, context) => {
    const {data, onChange, uniqueIdList, maxIdValue, showAllAdvanced} = props;
    return (
        <div>
            {showAllAdvanced && <Row className="other-row space-below" type="flex" align="middle">
                <IdValue data={data} onChange={onChange} uniqueIdList={uniqueIdList} maxIdValue={maxIdValue}
                         className="advanced-property" />
            </Row>}
            <Row className="other-row space-below" type="flex" align="middle">
                <Col span={8} className="toggle-field-container label-space">
                    <Switch
                        checked={data.uniqueItems}
                        placeholder="uniqueItems"
                        onChange={e => changeOtherValue(e, 'uniqueItems', data, onChange)}
                    />
                    <div className="system-label">
                        {LocalProvider('uniqueItems')}
                        <Tooltip title={LocalProvider('unique_items')}>
                            <i className="far fa-circle-question help-icon" />
                        </Tooltip>
                    </div>
                </Col>
                <Col span={8}>
                    <div className="system-label">
                        {LocalProvider('min_items')}
                    </div>
                    <div className="system-input-container size-s value-m">
                        <InputNumber
                            value={data.minItems}
                            placeholder="minItems"
                            onChange={e => changeOtherValue(e, 'minItems', data, onChange)}
                            className="full"
                        />
                    </div>
                </Col>
                <Col span={8}>
                    <div className="system-label">
                        {LocalProvider('max_items')}
                    </div>
                    <div className="system-input-container size-s value-m">
                        <InputNumber
                            value={data.maxItems}
                            placeholder="maxItems"
                            onChange={e => changeOtherValue(e, 'maxItems', data, onChange)}
                            className="full"
                        />
                    </div>
                </Col>
            </Row>
            <Row className="other-row" type="flex" align="middle">
                <Col span={24}>
                    <div className='system-label'>
                        {LocalProvider('tags_label')}
                        <Tooltip title={LocalProvider('tags_help')}>
                            <i className="far fa-circle-question help-icon" />
                        </Tooltip>
                    </div>
                    <div className="system-input-container size-s value-m">
                        <Input
                            value={data.tags}
                            placeholder={LocalProvider('tags_placeholder')}
                            onChange={e => changeOtherValue(e.target.value, 'tags', data, onChange)}
                        />
                    </div>
                </Col>
            </Row>
            <Row className="other-row hide" type="flex" align="middle">
                <Col span={12}>
                    <div className="system-label">
                        {LocalProvider('flags_label')}
                        <Tooltip title={LocalProvider('flags_help')}>
                            <i className="far fa-circle-question help-icon" />
                        </Tooltip>
                    </div>
                    <div className="system-input-container size-s value-m">
                        <Input
                            value={data.flags}
                            placeholder={LocalProvider('flags_placeholder')}
                            onChange={e => changeOtherValue(
                                e.target.value,
                                'flags',
                                data,
                                onChange
                            )}
                        />
                    </div>
                </Col>
            </Row>
        </div>
    );
};

SchemaArray.contextTypes = {
    changeCustomValue: PropTypes.func
};

const SchemaFile = (props, context) => {
    const {data, onChange, uniqueIdList, maxIdValue, mineTypeList, showAllAdvanced, parentData} = props;
    let onChangeHandler = onChange;
    const multiple = parentData && parentData.type === 'array';
    if(multiple) {
        onChangeHandler = (changedData) => {
            parentData.items = changedData;
            onChange(parentData);
        }
    }
    return (
        <div>
            {showAllAdvanced && <Row className="other-row space-below" type="flex" align="middle">
                <IdValue data={data} onChange={onChangeHandler} uniqueIdList={uniqueIdList}
                         maxIdValue={maxIdValue} className="advanced-property" />
            </Row>}

            <Row className="other-row" type="flex" align="middle">
                <Col span={6}>
                    <FlagCheckbox data={data} onChange={onChangeHandler} name={"mandatory"} className="inverted" />
                </Col>
                {showAllAdvanced && <Col span={6}>
                    <FieldReadOnly data={data} onChange={onChangeHandler} className="inverted"/>
                </Col>}
                <Col span={6} className="toggle-field-container">
                    <Switch
                        checked={data.hasOwnProperty('antivirus') ? data.antivirus : true}
                        onChange={e => changeOtherValue(e, 'antivirus', data, onChangeHandler)}
                    />
                    <div className='system-label'>
                        {LocalProvider('file_antivirus')}
                    </div>
                </Col>
            </Row>

            <Row className="other-row" type="flex" align="middle">
                <Col span={24}>
                    <div className="system-label">
                        {LocalProvider('mime_type_label')}
                        <Tooltip title={LocalProvider('mime_type_help')}>
                            <i className="far fa-circle-question help-icon" />
                        </Tooltip>
                    </div>
                    <MimeTypeInput value={data.hasOwnProperty('consumes') && data.consumes ? data.consumes : []}
                                   mineTypeList={mineTypeList}
                                   onChange={value => {
                                       changeOtherValue(value.length > 0 ? value : undefined, 'consumes', data, onChangeHandler);
                                   }}/>
                </Col>
            </Row>

            <Row className="other-row hide" type="flex" align="middle">
                <Col span={12}>
                    <div className="system-label">
                        {LocalProvider('flags_label')}
                        <Tooltip title={LocalProvider('flags_help')}>
                            <i className="far fa-circle-question help-icon" />
                        </Tooltip>
                    </div>
                    <div className="system-input-container size-s value-m">
                        <Input
                            value={data.flags}
                            placeholder={LocalProvider('flags_placeholder')}
                            onChange={e => changeOtherValue(
                                e.target.value,
                                'flags',
                                data,
                                onChangeHandler
                            )}
                        />
                    </div>
                </Col>
            </Row>

            <Row className="other-row" type="flex" align="middle">
                <Col span={24} className="toggle-field-container">
                    <Switch
                        checked={multiple}
                        onChange={e => {
                            if(e) {
                                onChange({
                                    type: 'array',
                                    items: data
                                });
                            }
                            else {
                                onChange(data);
                            }
                        }}
                    />
                    <div className='system-label'>
                        {LocalProvider('multiple')}
                    </div>
                </Col>
            </Row>
        </div>
    );
};

const SchemaCatalog = (props, context) => {
    const {data, onChange, uniqueIdList, maxIdValue, mineTypeList, showAllAdvanced, parentData} = props;
    let onChangeHandler = onChange;
    const multiple = parentData && parentData.type === 'array';
    if(multiple) {
        onChangeHandler = (changedData) => {
            parentData.items = changedData;
            onChange(parentData);
        }
    }

    return (
        <div>
            {showAllAdvanced && <Row className="other-row space-below" type="flex" align="middle">
                <IdValue data={data} onChange={onChangeHandler} uniqueIdList={uniqueIdList}
                         maxIdValue={maxIdValue} className="advanced-property" />
            </Row>}
            <Row className="other-row" type="flex" align="middle">
                {data.properties && data.properties.key && <Col span={6}>
                    <FlagCheckbox data={data.properties.key} onChange={d => {
                        data.properties.key = d;
                        onChangeHandler(data)
                    }} name={"unique"} className="inverted"/>
                </Col>}
                <Col span={6}>
                    <FlagCheckbox data={data} onChange={onChangeHandler} name={"mandatory"} className="inverted" />
                </Col>
                {showAllAdvanced && <Col span={6}>
                    <FieldReadOnly data={data} onChange={onChangeHandler} className="inverted"/>
                </Col>}
            </Row>
            <Row className="other-row" type="flex" align="middle">
                <Col span={6} className="toggle-field-container">
                    <Switch
                        checked={data.hasOwnProperty('showKeyAndValue') ? data.showKeyAndValue : false}
                        onChange={e => changeOtherValue(e, 'showKeyAndValue', data, onChangeHandler)}
                    />
                    <div className='system-label'>
                        {LocalProvider('catalog.showKeyAndValue')}
                    </div>
                </Col>
            </Row>
            <Row className="other-row" type="flex" align="middle">
                <Col span={8} className="advanced-property">
                    <Switch
                        checked={data.isFullText}
                        onChange={e => changeOtherValue(e, 'isFullText', data, onChangeHandler)}
                    />
                    <div className="system-label">
                        {LocalProvider('full_text_label')}
                        <Tooltip title={LocalProvider('full_text_help')}>
                            <Icon type="question-circle-o" style={{width: '10px', marginLeft: '4px'}}/>
                        </Tooltip>
                    </div>
                </Col>
            </Row>
            <Row className="other-row" type="flex" align="middle">
                <Col span={6}>
                    <Switch
                        checked={multiple}
                        onChange={e => {
                            if(e) {
                                onChange({
                                    type: 'array',
                                    items: data
                                });
                            }
                            else {
                                onChange(data);
                            }
                        }}
                    />
                    <div className="system-label">
                        {LocalProvider('multiple')}
                    </div>
                </Col>
            </Row>

        </div>
    );
};

const SchemaObject = (props, context) => {
    const {data, onChange, uniqueIdList, maxIdValue, mineTypeList, showAllAdvanced} = props;

    return (
        <div>
            {showAllAdvanced && <Row className="other-row space-below" type="flex" align="middle">
                <IdValue data={data} onChange={onChange} uniqueIdList={uniqueIdList}
                         maxIdValue={maxIdValue} className="advanced-property" />
            </Row>}
            <Row className="other-row" type="flex" align="middle">
                <Col span={6}>
                    <FlagCheckbox data={data} onChange={onChange} name={"mandatory"} className="inverted" />
                </Col>
                {showAllAdvanced && <Col span={6}>
                    <FieldReadOnly data={data} onChange={onChange} className="inverted"/>
                </Col>}
            </Row>
        </div>
    );
};

SchemaFile.contextTypes = {
    changeCustomValue: PropTypes.func
};

const mapping = (data, onChange, uniqueIdList, maxIdValue, databases, mineTypeList, showAllAdvanced) => {
    let dataType = data.type;
    if(dataType === 'array') {
        if(data.items && data.items.type === 'file') {
            dataType = 'array_of_file';
        }
        else if(data.items && data.items.type === 'catalog') {
            dataType = 'array_of_catalog';
        }
    }

    return {
        string: <SchemaString showAllAdvanced={showAllAdvanced} data={data} onChange={onChange}
                              uniqueIdList={uniqueIdList} maxIdValue={maxIdValue}
                              databases={databases}/>,
        number: <SchemaNumber showAllAdvanced={showAllAdvanced} data={data} onChange={onChange}
                              uniqueIdList={uniqueIdList} maxIdValue={maxIdValue}
                              databases={databases}/>,
        boolean: <SchemaBoolean showAllAdvanced={showAllAdvanced} data={data} onChange={onChange}
                                uniqueIdList={uniqueIdList} maxIdValue={maxIdValue}
                                databases={databases}/>,
        integer: <SchemaNumber showAllAdvanced={showAllAdvanced} data={data} onChange={onChange}
                               uniqueIdList={uniqueIdList} maxIdValue={maxIdValue}
                               databases={databases}/>,
        array: <SchemaArray showAllAdvanced={showAllAdvanced} data={data} onChange={onChange}
                            uniqueIdList={uniqueIdList} maxIdValue={maxIdValue}
                            databases={databases}/>,
        file: <SchemaFile showAllAdvanced={showAllAdvanced} data={data} onChange={onChange} uniqueIdList={uniqueIdList}
                          maxIdValue={maxIdValue}
                          databases={databases} mineTypeList={mineTypeList}/>,
        array_of_file: <SchemaFile showAllAdvanced={showAllAdvanced} parentData={data} data={data.items} onChange={onChange} uniqueIdList={uniqueIdList}
                          maxIdValue={maxIdValue}
                          databases={databases} mineTypeList={mineTypeList}/>,
        catalog: <SchemaCatalog showAllAdvanced={showAllAdvanced} data={data} onChange={onChange}
                            uniqueIdList={uniqueIdList} maxIdValue={maxIdValue}
                            databases={databases}/>,
        array_of_catalog: <SchemaCatalog showAllAdvanced={showAllAdvanced} parentData={data} data={data.items} onChange={onChange}
                                uniqueIdList={uniqueIdList} maxIdValue={maxIdValue}
                                databases={databases}/>,
        object: <SchemaObject showAllAdvanced={showAllAdvanced} data={data} onChange={onChange}
                            uniqueIdList={uniqueIdList} maxIdValue={maxIdValue}
                            databases={databases}/>,
    }[dataType];

};

const CustomItem = (props, context) => {
    const {data, onChange, uniqueIdList, maxIdValue, databases, mineTypeList, fields, showAllAdvanced, triggerAdditionalValueOptions, toggleAllAdvanced} = props;
    const optionForm = mapping(data, onChange, uniqueIdList, maxIdValue, databases, mineTypeList, showAllAdvanced);
    let triggerVisible = ['string', 'number', 'integer', 'boolean', 'catalog'].some(t => data.type === t);
    if (data.type === 'array') {
        if (data.items && data.items.type === 'catalog') {
            triggerVisible = true;
        }
    }
    if (triggerVisible || showAllAdvanced) {
        return (
            <Tabs type="card">
                <Tabs.TabPane tab={LocalProvider('base_setting')} key="base-setting">
                    <Row className='advanced-toggle-row'>
                        <div className='toggle-field-container'>
                            <Switch
                                checked={showAllAdvanced}
                                checkedChildren={<i className="far fa-eye" />}
                                unCheckedChildren={<i className="far fa-eye-slash" />}
                                onChange={toggleAllAdvanced}
                            />
                            <div className='system-label'>
                                {LocalProvider('simple_or_advanced_view')}
                            </div>
                        </div>
                    </Row>
                    {optionForm}
                    <div className="bottom-space"></div>
                </Tabs.TabPane>
                {triggerVisible &&
                <Tabs.TabPane tab={LocalProvider('trigger_setting')} key="trigger-setting">
                    <SchemaTrigger data={data} onChange={onChange} fields={fields}
                                   additionalValueOptions={triggerAdditionalValueOptions}/>
                    <div className="bottom-space"></div>
                </Tabs.TabPane>}
                <Tabs.TabPane tab={LocalProvider('description')} key="description">
                    <SchemaDescription data={data} onChange={onChange}/>
                    <div className="bottom-space"></div>
                </Tabs.TabPane>
                {showAllAdvanced && <Tabs.TabPane tab={LocalProvider('all_setting')} key="all-setting">
                    <AceEditor
                        data={JSON.stringify(data, null, 2)}
                        mode="json"
                        onChange={e => onChange(e.jsonData)}
                    />
                    <div className="bottom-space"></div>
                </Tabs.TabPane>}
            </Tabs>
        )
    }
    return (
        <div>
            {optionForm}
        </div>
    );
};

CustomItem.contextTypes = {
    changeCustomValue: PropTypes.func
};

export default CustomItem;
