import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {Input} from 'antd';
import LocalProvider from "../LocalProvider";
const {TextArea} = Input;

export default class SchemaDescription extends PureComponent {
    static propTypes = {
        onChange: PropTypes.func,
        data: PropTypes.object,
    };

    render() {
        const {description, example} = this.props.data;
        return (
            <div>
                <div className='textarea system-input-container size-s value-m'>
                    <TextArea
                        value={description || ''}
                        placeholder={LocalProvider('description')}
                        onChange={e => {
                            this.props.data.description = e.target.value;
                            this.props.onChange(this.props.data);
                        }}
                        autoSize={{minRows: 6, maxRows: 10}}
                    />
                </div>
                <div className='mt-4'>
                    <label className='system-label'>{LocalProvider('example')}</label>
                    <div className='system-input-container size-s value-m'>
                        <Input value={example || ''} onChange={e=> {
                            this.props.data.example = e.target.value;
                            this.props.onChange(this.props.data);
                        }}/>
                    </div>
                </div>
            </div>
        )
    }
}
