import React from 'react';
import {
    Icon,
    Input,
    Modal,
    Switch,
    Tooltip
} from 'antd';

const {TextArea} = Input;
import './schemaJson.css';
import {cloneObject, getSchemaRealType} from '../../utils';
import Nestable from "../NestableDrag/Nestable";
import {SchemaItem} from "./SchemaItem";
import CustomItem from "./SchemaOther";
import LocalProvider from "../LocalProvider";

export class SchemaArrayChild extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.onSwitch = this.onSwitch.bind(this);
        this.showAdvanced = this.showAdvanced.bind(this);
        this.handleAdvOk = this.handleAdvOk.bind(this);
        this.handleAdvCancel = this.handleAdvCancel.bind(this);
        this.onChangeAdvanced = this.onChangeAdvanced.bind(this);
        this.onChangeIncrementIndex = this.onChangeIncrementIndex.bind(this);
        this.toggleAllAdvanced = this.toggleAllAdvanced.bind(this);
        this.nestableRenderItem = this.nestableRenderItem.bind(this);
        this.nestableConfirmChange = this.nestableConfirmChange.bind(this);
        this.onChangeCollapsedNestable = this.onChangeCollapsedNestable.bind(this);
        this.toggleCollapsedAll = this.toggleCollapsedAll.bind(this);
        this.state = {
            items: props.items,
            fields: SchemaArrayChild.getFields(props.items),
            advanced: null,
            simpleView: false,
            showAllAdvanced: props.settings.showAllAdvanced === undefined ? false : props.settings.showAllAdvanced,
            collapsedIds: []
        };
        for (const item of props.items) {
            if (Array.isArray(item.children) && item.children.length > 0) {
                this.state.collapsedIds.push(item.id);
            }
        }
        this.nestedItemHandler = (
            <button type='button' className='system-button size-s minimal grayscale icon-button' style={{cursor: 'move'}}>
                <i className='button-icon far fa-arrows-up-down-left-right' />
            </button>
        );
        this.incrementIndex = props.incrementIndex;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.incrementIndex !== this.props.incrementIndex) {
            this.incrementIndex = this.props.incrementIndex;
            this.forceUpdate();
        }
    }

    static getDerivedStateFromProps(props, state) {
        let collapsedIds = state.collapsedIds || [];
        if (state.items.length === 0 && collapsedIds.length === 0 && props.items.length > 0) {
            for (const item of props.items) {
                if (Array.isArray(item.children) && item.children.length > 0) {
                    collapsedIds.push(item.id);
                }
            }
            if (collapsedIds.length > 0) {
                collapsedIds = [...collapsedIds];
            }
        }
        return {
            collapsedIds,
            items: props.items,
            fields: SchemaArrayChild.getFields(props.items),
            showAllAdvanced: props.settings && props.settings.showAllAdvanced != null ? props.settings.showAllAdvanced : false
        };
    }

    onSwitch() {
        this.setState({simpleView: !this.state.simpleView});
    }

    render() {
        const {items, simpleView, collapsedIds} = this.state;
        const itemsHasChildren = items.filter(item => Array.isArray(item.children) && item.children.length > 0);
        const isCollapsedAll = itemsHasChildren.every(item => collapsedIds && collapsedIds.indexOf(item.id) !== -1);

        return (
            <div className={simpleView ? 'simple-view schema-content' : 'schema-content'}>
                <div className='d-flex align-items-center justify-content-between'>
                    {itemsHasChildren.length > 0 && <button className="system-button size-s text grayscale" onClick={this.toggleCollapsedAll}>
                        <i className={isCollapsedAll ? 'far fa-angle-down collapse-icon' : 'far fa-angle-up collapse-icon'}/>
                        {LocalProvider(isCollapsedAll ? 'expand.all' : 'collapse.all')}
                    </button>}
                    {this.props.showSimpleViewButton && <label className='simple-view-toggle-container'>
                        <Switch onChange={this.onSwitch} className="simple-view-switch" />
                        { LocalProvider('simple_view') }
                    </label>}
                </div>
                <Nestable
                    incrementIndex={this.incrementIndex}
                    items={items}
                    renderItem={this.nestableRenderItem}
                    handler={this.nestedItemHandler}
                    onChange={this.onChange}
                    onChangeIncrementIndex={this.onChangeIncrementIndex}
                    confirmChange={this.nestableConfirmChange}
                    onChangeCollapsed={this.onChangeCollapsedNestable}
                    collapsedIds={collapsedIds}
                />
                {this.renderAdvancedModal()}
            </div>
        );
    }

    renderAdvancedModal() {
        if(!this.state.advanced) {
            return  '';
        }
        const {advanced, showAllAdvanced} = this.state;
        if(typeof this.props.renderAdvancedModal === 'function') {
            return this.props.renderAdvancedModal({
                header: (
                    <div>
                        <span>'{this.state.advanced.item.key}'</span>
                        <span>{LocalProvider('adv_setting')}</span>
                        {/*<div className={"all-advanced-visible-switch"}>*/}
                        {/*    <Tooltip title={LocalProvider('simple_or_advanced_view')}>*/}
                        {/*        <Switch*/}
                        {/*            checked={showAllAdvanced}*/}
                        {/*            checkedChildren={<Icon type="eye" />}*/}
                        {/*            unCheckedChildren={<Icon type="eye-invisible" />}*/}
                        {/*            onChange={this.toggleAllAdvanced}*/}
                        {/*        />*/}
                        {/*    </Tooltip>*/}
                        {/*</div>*/}
                    </div>
                ),
                body: (
                    <CustomItem showAllAdvanced={showAllAdvanced}
                                triggerAdditionalValueOptions={this.props.triggerAdditionalValueOptions}
                                fields={[...this.props.additionalFields, ...this.state.fields]}
                                databases={this.props.databases}
                                mineTypeList={this.props.mineTypeList}
                                data={advanced.data}
                                onChange={this.onChangeAdvanced}
                                uniqueIdList={advanced.uniqueIdList}
                                maxIdValue={this.incrementIndex}
                                toggleAllAdvanced={this.toggleAllAdvanced}
                    />
                ),
                onOk: this.handleAdvOk,
                onCancel: this.handleAdvCancel
            })
        }
        return (
            <Modal
                title={<div>
                    <span className="ml-1">'{this.state.advanced.item.key}'</span>
                    <span>{LocalProvider('adv_setting')}</span>
                    {/*<div className={"all-advanced-visible-switch"}>*/}
                    {/*    <Tooltip title={LocalProvider('simple_or_advanced_view')}>*/}
                    {/*        <Switch*/}
                    {/*            checked={showAllAdvanced}*/}
                    {/*            checkedChildren={<Icon type="eye" />}*/}
                    {/*            unCheckedChildren={<Icon type="eye-invisible" />}*/}
                    {/*            onChange={this.toggleAllAdvanced}*/}
                    {/*        />*/}
                    {/*    </Tooltip>*/}
                    {/*</div>*/}
                </div>}
                maskClosable={false}
                visible={true}
                onOk={this.handleAdvOk}
                onCancel={this.handleAdvCancel}
                okText={LocalProvider('ok')}
                width={780}
                cancelText={LocalProvider('cancel')}
                className="json-schema-react-editor-adv-modal">
                <CustomItem showAllAdvanced={showAllAdvanced}
                            triggerAdditionalValueOptions={this.props.triggerAdditionalValueOptions}
                            fields={[...this.props.additionalFields, ...this.state.fields]}
                            databases={this.props.databases} mineTypeList={this.props.mineTypeList}
                            data={advanced.data}
                            onChange={this.onChangeAdvanced}
                            uniqueIdList={advanced.uniqueIdList}
                            maxIdValue={this.incrementIndex}
                            toggleAllAdvanced={this.toggleAllAdvanced}
                />
            </Modal>
        )
    }

    toggleCollapsedAll() {
        const {items} = this.state;
        let {collapsedIds} = this.state;
        const itemsHasChildren = items.filter(item => Array.isArray(item.children) && item.children.length > 0);
        const isCollapsedAll = itemsHasChildren.every(item => collapsedIds && collapsedIds.indexOf(item.id) !== -1);
        if(isCollapsedAll) {
            this.setState({collapsedIds: []});
        }
        else {
            if(!collapsedIds) {
                collapsedIds = [];
            }
            itemsHasChildren.forEach(item => {
                if(collapsedIds.indexOf(item.id) === -1) {
                    collapsedIds.push(item.id);
                }
            });
            this.setState({collapsedIds: [...collapsedIds]});
        }
    }

    onChangeCollapsedNestable(collapsedIds) {
        this.setState({collapsedIds});
    }

    nestableRenderItem({item, handler, onChange, parent, addSiblingNode, addChildNode, deleteItem, collapseIcon, refX}) {
        return (
            <SchemaItem
                refX={refX}
                customDuplicateFieldButton={this.props.customDuplicateFieldButton}
                customAddFieldButton={this.props.customAddFieldButton}
                customSettingButton={this.props.customSettingButton}
                customDescriptionButton={this.props.customDescriptionButton}
                dragHandler={handler}
                item={item}
                onChange={onChange}
                parent={parent}
                addSiblingNode={addSiblingNode}
                addChildNode={addChildNode}
                deleteItem={deleteItem}
                showAdvanced={this.showAdvanced}
                collapseIcon={collapseIcon}
                reservedFieldKeyRegex={this.props.reservedFieldKeyRegex}
                lockedFieldTypeIds={this.props.lockedFieldTypeIds}
                reservedFieldIds={this.props.reservedFieldIds}
            />
        )
    }

    nestableConfirmChange(dragItem, destinationParent) {
        if (destinationParent && destinationParent.data && (['array', 'object'].indexOf(destinationParent.data.type) === -1)) {
            return false;
        }
        const parentChild = this.getParentChild(this.state.items, dragItem.id);
        if (destinationParent && parentChild !== undefined) {
            const parentChildren = (parentChild === null ? this.state.items : parentChild.children).filter(item => item.data.type === 'object');
            if (this.checkMoveChild(parentChildren, destinationParent)) {
                return true;
            }
        }
        return this.checkMoveParent(parentChild, destinationParent);
    }

    toggleAllAdvanced(visible) {
        this.setState({showAllAdvanced: visible});
        let settings = this.props.settings;
        settings.showAllAdvanced = visible;
        this.props.onChangeSettings(settings);
    }

    getIdList(items) {
        let ids = [];
        items.forEach(item => {
            ids.push(item.id);
            if(item.hasOwnProperty('children')) {
                ids = [...ids, ...this.getIdList(item.children)]
            }
        });
        return ids;
    }

    onChangeIncrementIndex(index) {
        this.incrementIndex = index;
    }

    getParentChild(items, id) {
        for (const item of items) {
            if (item.id === id) {
                return null;
            }
            if (item.children && item.children.length > 0) {
                const parentItem = this.getParentChild(item.children, id);
                if (parentItem === null) {
                    return item;
                } else if (parentItem) {
                    return parentItem;
                }
            }
        }
        return undefined;
    }

    checkMoveParent(parentChild, destinationParent) {
        if (parentChild === null && destinationParent === null || parentChild && destinationParent && parentChild.id === destinationParent.id) {
            return true;
        } else if (parentChild) {
            if (parentChild.data.type === 'array') {
                return false;
            }
            if (destinationParent && parentChild.children && this.checkMoveChild(parentChild.children.filter(item => item.data.type === 'object'), destinationParent)) {
                return true;
            }
            return this.checkMoveParent(this.getParentChild(this.state.items, parentChild.id), destinationParent);
        } else if (parentChild == null && destinationParent && this.checkMoveChild(this.state.items.filter(item => item.data.type === 'object'), destinationParent)) {
            return true;
        }
        return false;
    }

    checkMoveChild(items, destinationItem) {
        for (const item of items) {
            if (item.id === destinationItem.id) {
                return true;
            }
            if (item.children && this.checkMoveChild(item.children.filter(item => item.data.type === 'object'), destinationItem)) {
                return true;
            }
        }
        return false;
    }

    onChangeAdvanced(data) {
        let {advanced} = this.state;
        if(!Number.isInteger(data['$id'])) {
            data['$id'] = advanced.item.id;
        }
        advanced.data = {...data};
        this.setState({advanced});
    }

    showAdvanced(item, onChange) {
        let data = {...cloneObject(item.data), '$id': item.id};
        let flags = data.flags ? data.flags.split(',') : [];
        if(item.required && !flags.includes('mandatory')) {
            flags.push('mandatory');
        }
        else if(!item.required && flags.includes('mandatory')) {
            flags = flags.filter(f => f !== 'mandatory');
        }
        data.flags = flags.join(',');
        this.setState({
            advanced: {
                item: item,
                uniqueIdList: this.getIdList(this.state.items).filter(id => item.id !== id),
                data,
                onChange
            }
        });
    }

    handleAdvCancel() {
        this.setState({advanced: null});
    }

    handleAdvOk() {
        const advanced = this.state.advanced;
        let item = advanced.item;
        item.data = advanced.data;
        if(item.data['$id'] <= this.incrementIndex && advanced.uniqueIdList.filter(id => id === item.data['$id']).length === 0) {
            item.id = item.data['$id'];
        }
        if(item.data.type === 'array' && item.data.items && (item.data.items.type === 'file' || item.data.items.type === 'catalog')) {
            item.data.flags = item.data.items.flags;
        }
        let flags = item.data.flags ? item.data.flags.split(',').map(f => f.trim()) : [];
        if(item.data.primaryKey) {
            item.required = true;
            if(flags.indexOf('unique') === -1) {
                flags.push('unique');
            }
            if(flags.indexOf('mandatory') === -1) {
                flags.push('mandatory');
            }
        }
        item.required = flags.indexOf('mandatory') !== -1;
        item.data.flags = flags.join(', ');
        if(item.data.foreignKeys) {
            item.data.foreignKeys = item.data.foreignKeys.filter(foreignKey => foreignKey.databaseKey && foreignKey.databaseKey !== '');
        }
        delete item.data['$id'];
        advanced.onChange(item);
        this.setState({advanced: null});
    }

    onChange(data) {
        this.setState({items: data, fields: SchemaArrayChild.getFields(data)});
        this.props.onChangeData(data, this.incrementIndex);
    }

    static getFields(schemaData, path=[]) {
        let fields = [];
        for(const item of schemaData) {
            const _path = [...path, item.key];
            if(item.data.type === 'array') {
                _path.push('*')
            }
            if(item.children && item.children.length > 0) {
                fields = [...fields, ...SchemaArrayChild.getFields(item.children, _path)]
            }
            else if(item.data.type === 'catalog') {
                if (item.data.properties) {
                    for(const subKey of Object.keys(item.data.properties)) {
                        const subField = item.data.properties[subKey];
                        if (subField['$id']) {
                            fields.push({
                                field_id: subField['$id'],
                                path: [..._path, subKey],
                                type: getSchemaRealType(subField)
                            })
                        }
                    }
                }
            }
            else {
                fields.push({
                    field_id: item.id,
                    path: _path,
                    type: getSchemaRealType(item.data)
                });
            }
        }
        return fields;
    }
}
