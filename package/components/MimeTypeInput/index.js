import React from 'react';
import {Component} from 'react'
import LocalProvider from "../LocalProvider";
import {Typeahead} from "react-bootstrap-typeahead";
import "react-bootstrap-typeahead/css/Typeahead.css";

export default class MimeTypeInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.formatValue(props.value, props.mineTypeList)
        };
        this.onChange = this.onChange.bind(this);
    }

    UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
        this.setState({value: this.formatValue(nextProps.value, nextProps.mineTypeList)});
    }

    formatValue(propsValue, mineTypeList) {
        const newValue = propsValue.map(v => {
            const mime = mineTypeList.filter(m => m.list.indexOf(v) !== -1);
            return mime.length === 0 ? {id: v, name: v, list: [v]} : mime[0];
        });
        let value = [];
        newValue.forEach(itemValue => {
            if(!value.some(v => v.id === itemValue.id)) {
                value.push(itemValue);
            }
        });
        return value;
    }

    render() {
        return (
            <Typeahead
                id="mime-type-id"
                clearButton
                selected={this.state.value}
                labelKey="name"
                multiple
                options={this.props.mineTypeList}
                placeholder={LocalProvider('mime_type_placeholder')}
                onChange={this.onChange}
            />
        );
    }

    onChange(newValue) {
        this.setState({value: newValue});
        let list = [];
        newValue.map(m => typeof (m) === "object" ? (m.list ? m.list : m.id) : m).forEach(x => Array.isArray(x) ? list.push(...x) : list.push(x));
        this.props.onChange(list);
    }
}