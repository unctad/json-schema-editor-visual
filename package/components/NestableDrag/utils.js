export const objectType = (obj) => {
    return Object.prototype.toString.call(obj).slice(8, -1);
};
export const isDefined = (param) => {
    return typeof param != "undefined";
};
export const isUndefined = (param) => {
    return typeof param == "undefined";
};
export const isFunction = (param) => {
    return typeof param == "function";
};
export const isNumber = (param) => {
    return typeof param == "number" && !isNaN(param);
};
export const isString = (str) => {
    return objectType(str) === "String";
};
export const isArray = (arr) => {
    return objectType(arr) === "Array";
};

export const closest = (target, selector) => {
    // closest(e.target, '.field')
    while (target) {
        if (target.matches && target.matches(selector)) return target;
        target = target.parentNode;
    }
    return null;
};

function getScrollParent(node) {
    if (node == null) {
        return null;
    }

    if (node.scrollHeight > node.clientHeight && node.scrollTop !== 0) {
        return node;
    }
    return getScrollParent(node.parentNode);
}

export const getOffsetRect = (elem) => {
    // (1)
    var box = elem.getBoundingClientRect();
    var body = document.body;
    var docElem = document.documentElement;

    var mainElement = getScrollParent(elem.parentNode);

    // (2)
    var scrollTop = mainElement && mainElement.scrollTop ? mainElement.scrollTop : 0;
    var scrollLeft = mainElement && mainElement.scrollLeft ? mainElement.scrollLeft : 0;

    // (3)
    var clientTop = mainElement && mainElement.clientTop ? mainElement.clientTop : 0;
    var clientLeft = mainElement && mainElement.clientLeft ? mainElement.clientLeft : 0;

    // (4)
    var top = box.top + scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;

    return {top: Math.round(top), left: Math.round(left)};
};

export const getTotalScroll = (elem) => {
    let top = 0;
    let left = 0;

    while ((elem = elem.parentNode)) {
        top += elem.scrollTop || 0;
        left += elem.scrollLeft || 0;
    }

    return {top, left};
};

export const getTransformProps = (x, y) => {
    return {
        transform: 'translate(' + x + 'px, ' + y + 'px)'
    };
};

export const listWithChildren = (list, childrenProp) => {
    return list.map(item => {
        return {
            ...item,
            [childrenProp]: item[childrenProp]
                ? listWithChildren(item[childrenProp], childrenProp)
                : []
        };
    });
};

export const getAllNonEmptyNodesIds = (items, childrenProp) => {
    let childrenIds = [];
    let ids = items
        .filter(item => item[childrenProp].length)
        .map(item => {
            childrenIds = childrenIds.concat(getAllNonEmptyNodesIds(item[childrenProp], childrenProp));
            return item.id;
        });

    return ids.concat(childrenIds);
};

export const getLatestIncrementIndex = (items) => {
    let maxVal = 0;
    items.forEach(item => {
        if (maxVal < item.id) {
            maxVal = item.id;
        }
        if (item.hasOwnProperty('children')) {
            const childIndex = getLatestIncrementIndex(item.children);
            if (maxVal < childIndex) {
                maxVal = childIndex;
            }
        }
    });
    return maxVal;
};
