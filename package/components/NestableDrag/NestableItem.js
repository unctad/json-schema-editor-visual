import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import {cloneObject} from "../../utils";
import {Icon} from "antd";

class NestableItem extends Component {

    static propTypes = {
        item: PropTypes.shape({
            id: PropTypes.any.isRequired
        }),
        isCopy: PropTypes.bool,
        options: PropTypes.object,
        index: PropTypes.number,
        onChange: PropTypes.func,
        addSiblingNode: PropTypes.func,
        getNextId: PropTypes.func,
        parent: PropTypes.array,
        deleteItem: PropTypes.func,
        refX: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.addChildNode = this.addChildNode.bind(this);
        this.refItem = null;
        this.childItemsRef = {};
        this.willFocusNewChildItemIndex = null;
    }

    componentDidMount() {
        if(this.props.refX) {
            this.props.refX(this);
        }
    }

    componentWillUnmount() {
        if(this.props.refX) {
            this.props.refX(undefined);
        }
    }

    renderCollapseIcon = ({isCollapsed}) => (
        <i className={cn("far", {"fa-angle-down": isCollapsed}, {"fa-angle-up": !isCollapsed})} />
    );

    generateItemId(item) {
        if (!item.hasOwnProperty('id')) {
            item.id = this.props.getNextId();
            this.generateIdNestedObj(item.data);
        }
        if (item.hasOwnProperty(this.props.options.childrenProp)) {
            item[this.props.options.childrenProp].forEach(childItem => this.generateItemId(childItem));
        }
    }

    generateIdNestedObj(obj) {
        if (typeof obj !== 'object' || !obj) {
            return;
        }
        if (obj['$id'] !== undefined) {
            obj['$id'] = this.props.getNextId();
        }
        if (obj.properties) {
            for (const key of Object.keys(obj.properties)) {
                this.generateIdNestedObj(obj.properties[key]);
            }
        }
        else if (obj.items) {
            this.generateIdNestedObj(obj.items);
        }
    }

    render() {
        const {isCopy, options, index, parent} = this.props;
        const {item} = this.props;
        const {
            dragItem,
            renderItem,
            handler,
            childrenProp,
            renderCollapseIcon = this.renderCollapseIcon
        } = options;
        const isCollapsed = options.isCollapsed(item);

        const isDragging = !isCopy && dragItem && dragItem.id === item.id;
        const hasChildren = item[childrenProp] && item[childrenProp].length > 0;

        let Handler;

        let itemProps = {
            className: cn(
                "nestable-item" + (isCopy ? '-copy' : ''),
                "nestable-item" + (isCopy ? '-copy' : '') + '-' + item.id,
                {
                    'is-dragging': isDragging
                }
            )
        };

        let rowProps = {};
        let handlerProps = {};
        if (!isCopy) {
            if (dragItem) {
                rowProps = {
                    ...rowProps,
                    onMouseEnter: (e) => options.onMouseEnter(e, item)
                };
            } else {
                handlerProps = {
                    ...handlerProps,
                    draggable: true,
                    onDragStart: (e) => options.onDragStart(e, item)
                };
            }
        }

        if (handler) {
            Handler = <span className="nestable-item-handler" {...handlerProps}>{handler}</span>;
            //Handler = React.cloneElement(handler, handlerProps);
        } else {
            rowProps = {
                ...rowProps,
                ...handlerProps
            };
        }

        const collapseIcon = hasChildren
            ? (
                <button className='system-button size-s minimal grayscale icon-button' onClick={() => options.onToggleCollapse(item)}>
                    {renderCollapseIcon({isCollapsed})}
                </button>
            )
            : null;

        const itemClassnames = [];
        if(item.data && item.data.type) {
            let t = `type-${item.data.type}`;
            if (item.data.type === 'array' && item.data.items && item.data.items.type) {
                t += `-of-${item.data.items.type}`;
            }
            itemClassnames.push(t)
        }
        return (
            <li {...itemProps}>
                <div className={cn("nestable-item-name", {'is-collapsed': isCollapsed}, ...itemClassnames)} {...rowProps}>
                    {renderItem({
                        refX: x => this.refItem = x,
                        item,
                        collapseIcon,
                        handler: Handler,
                        index,
                        onChange: this.onChange.bind(this),
                        parent,
                        addSiblingNode: this.props.addSiblingNode,
                        addChildNode: this.addChildNode,
                        deleteItem: this.props.deleteItem,
                    })}
                </div>

                {hasChildren && !isCollapsed && (
                    <ol className="nestable-list">
                        {item[childrenProp].map((childItem, i) => {
                            return (
                                <NestableItem
                                    refX={this.refXChildItem.bind(this, i)}
                                    parent={item[childrenProp]}
                                    key={i}
                                    index={i}
                                    item={childItem}
                                    options={options}
                                    isCopy={isCopy}
                                    onChange={updatedItem => {
                                        item[childrenProp][i] = updatedItem;
                                        this.onChange(item);
                                    }}
                                    addSiblingNode={newItem => {
                                        this.generateItemId(newItem);
                                        if (!newItem.hasOwnProperty(childrenProp)) {
                                            newItem[childrenProp] = [];
                                        }
                                        this.willFocusNewChildItemIndex = i + 1;
                                        item[childrenProp].splice(i + 1, 0, newItem);
                                        this.props.onChange(item);
                                        this.forceUpdate();
                                    }}
                                    getNextId={this.props.getNextId}
                                    deleteItem={() => {
                                        item[childrenProp].splice(i, 1);
                                        this.props.onChange(item);
                                        this.forceUpdate();
                                    }}
                                />
                            );
                        })}
                    </ol>
                )}
            </li>
        );
    }

    addChildNode(newItem) {
        let item = this.props.item;
        const {childrenProp} = this.props.options;
        if (!item.hasOwnProperty(childrenProp)) {
            item[childrenProp] = [];
        }
        if (!newItem.hasOwnProperty('id')) {
            newItem.id = this.props.getNextId();
        }
        if (!newItem.hasOwnProperty(childrenProp)) {
            newItem[childrenProp] = [];
        }
        this.willFocusNewChildItemIndex = item[childrenProp].length;
        item[childrenProp].push(newItem);
        this.props.onChange(item);
        this.forceUpdate();
    }


    onChange(item) {
        if (this.props.onChange) {
            this.props.onChange(cloneObject(item));
        }
    }

    refXChildItem(index, x) {
        this.childItemsRef[index] = x;
        if(this.willFocusNewChildItemIndex !== null && this.childItemsRef[this.willFocusNewChildItemIndex]) {
            this.childItemsRef[this.willFocusNewChildItemIndex].focus();
            this.willFocusNewChildItemIndex = null;
        }
    }

    focus() {
        if(this.refItem && this.refItem.focus) {
            this.refItem.focus();
        }
    }
}

export default NestableItem;
