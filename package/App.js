import React from 'react';
import {
    Row,
    Col,
} from 'antd';


import './index.css';
import AceEditor from './components/AceEditor/AceEditor.js';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {debounce} from './utils.js';
import handleSchema from './schema';

const utils = require('./utils');
import LocalProvider from './components/LocalProvider/index.js';
import {SchemaArrayChild} from "./components/SchemaComponents/SchemaJson";
import {cloneObject, validateSchema} from "./utils";
import {getLatestIncrementIndex} from "./components/NestableDrag/utils";

class jsonSchema extends React.Component {
    constructor(props) {
        super(props);
        this.alterMsg = debounce(this.alterMsg, 2000);
        this.state = {
            visible: false,
            show: true,
            editVisible: false,
            description: '',
            descriptionKey: null,
            advVisible: false,
            itemKey: [],
            curItemCustomValue: null,
            checked: false,
            editorModalName: '', // 弹窗名称desctiption | mock
            mock: '',
            schema: [],
        };
        this.Model = this.props.Model.schema;
        this.jsonSchemaData = null;
        this.jsonData = null;
        this.jsonSchema = null;
        this.onChangeSettings = this.onChangeSettings.bind(this);
        this.onChangeSchema = this.onChangeSchema.bind(this);
        this.exportJson = this.exportJson.bind(this);
        this.importJson = this.importJson.bind(this);
    }

    importJson() {
        this.uploadJsonFile.click();
    }

    cleanSchemaForImportOrExport(schema) {
        if (schema.foreignKeys) {
            delete schema.foreignKeys;
        }
        if (schema.type === 'object' && schema.properties) {
            for (const subSchema of Object.values(schema.properties)) {
                this.cleanSchemaForImportOrExport(subSchema);
            }
        }
        else if (schema.type === 'array' && schema.items) {
            this.cleanSchemaForImportOrExport(schema.items);
        }
    }

    exportJson() {
        const schema = JSON.parse(JSON.stringify(this.props.schema));
        this.cleanSchemaForImportOrExport(schema);
        let link = document.createElement('a');
        let blob = new Blob([JSON.stringify(schema)], {type: 'application/json;charset=utf-8'});
        link.href = URL.createObjectURL(blob);
        link.download  = 'schema.json';
        link.click();
        URL.revokeObjectURL(link.href);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        let newData = JSON.stringify(nextProps.schema || '');
        const currentData = (JSON.stringify(this.jsonSchema || ''));
        if(newData !== currentData) {
            let schemaData = this.convertSchemaToChildArray(nextProps.schema);
            const latestIncrementIndex = getLatestIncrementIndex(schemaData);
            let incrementIndex = nextProps.schema.hasOwnProperty('$incrementIndex') ? nextProps.schema['$incrementIndex'] : latestIncrementIndex;
            if(latestIncrementIndex > incrementIndex) {
                incrementIndex = latestIncrementIndex;
            }
            this.formatIdChildArray(schemaData, incrementIndex);
            this.setState({schema: schemaData});
            this.jsonSchema = nextProps.schema;
        }
        if (typeof this.props.onChange === 'function' && this.props.schema !== nextProps.schema) {
            let oldData = JSON.stringify(this.props.schema || '');
            let newData = JSON.stringify(nextProps.schema || '');
            if (oldData !== newData) {
                return this.props.onChange(newData);
            }
        }
        if (this.props.data && this.props.data !== nextProps.data) {
            this.Model.changeEditorSchemaAction({value: JSON.parse(nextProps.data)});
        }
    }

    UNSAFE_componentWillMount() {
        let data = this.props.data;
        if (!data) {
            data = `{
        "type": "object",
        "properties":{
        "field_1": {
          "type": "string"
          }
        }
      }`;
        }
        this.Model.changeEditorSchemaAction({value: JSON.parse(data)});
        this.uploadJsonFile = document.createElement('input');
        this.uploadJsonFile.setAttribute('type', 'file');
        this.uploadJsonFile.setAttribute('accept', 'application/json');
        this.uploadJsonFile.onchange = (event) => {
            for (const file of event.target.files) {
                let reader = new FileReader();
                reader.onload = (e) => {
                    let data = null;
                    try {
                        const jsonContent = decodeURIComponent(window.escape(window.atob(e.target.result.split(",")[1])));
                        data = JSON.parse(jsonContent);
                    }
                    catch (e) {
                        this.invokeError({
                            code: "INVALID_FILE_FORMAT",
                            message: LocalProvider('invalid_file_format'),
                            ex: e
                        })
                    }
                    if(data) {
                        if(validateSchema(data)) {
                            this.cleanSchemaForImportOrExport(data);
                            this.Model.changeEditorSchemaAction({value: data});
                        }
                        else {
                            this.invokeError({
                                code: "INVALID_SCHEMA",
                                message: LocalProvider('invalid_schema')
                            })
                        }
                    }
                };
                reader.readAsDataURL(file);
            }
        };
    }

    invokeError(err) {
        if(this.props.onError) {
            this.props.onError(err);
        }
        else {
            console.error(err.message)
        }
    }

    getChildContext() {
        return {
            getOpenValue: keys => {
                return utils.getData(this.props.open, keys);
            },
            changeCustomValue: this.changeCustomValue,
            Model: this.props.Model,
            isMock: this.props.isMock
        };
    }

    // AceEditor 中的数据
    handleParams = e => {
        if (!e.text) return;
        // 将数据map 到store中
        if (e.format !== true) {
            return;
        }
        handleSchema(e.jsonData);
        this.Model.changeEditorSchemaAction({
            value: e.jsonData
        });
    };

    handleImportJson = e => {
        if (!e.text || e.format !== true) {
            return (this.jsonData = null);
        }
        this.jsonData = e.jsonData;
        this.setState({importJsonData: JSON.stringify(this.jsonData)});
    };

    // 修改备注信息
    changeValue = (key, value) => {
        if (key[0] === 'mock') {
            value = value ? {mock: value} : '';
        }
        this.Model.changeValueAction({key, value});
    };

    // 备注/mock弹窗 点击ok 时
    handleEditOk = name => {
        this.setState({
            editVisible: false
        });
        let value = this.state[name];
        if (name === 'mock') {
            value = value ? {mock: value} : '';
        }
        this.Model.changeValueAction({key: this.state.descriptionKey, value});
    };

    changeCheckBox = e => {
        this.setState({checked: e});
        this.Model.requireAllAction({required: e, value: this.props.schema});
    };

    render() {
        const {schema, settings} = this.props;
        const latestIncrementIndex = getLatestIncrementIndex(this.state.schema);
        let incrementIndex = schema.hasOwnProperty('$incrementIndex') ? schema['$incrementIndex'] : latestIncrementIndex;
        if(latestIncrementIndex > incrementIndex) {
            incrementIndex = latestIncrementIndex;
        }
        let reservedFieldKeyRegex = undefined;
        if(this.props.reservedFieldKeyPattern) {
            try{
                reservedFieldKeyRegex = new RegExp(this.props.reservedFieldKeyPattern);
            }
            catch (ex) {}
        }
        return (
            <div className="json-schema-react-editor">
                <div className="schema-editor-header page-panel-heading d-flex align-items-center">
                    <div className="panel-heading-title">{this.props.leftHeader}</div>
                    <button className="system-button size-s text" onClick={this.importJson}>
                        <i className="far fa-arrow-up-from-bracket" />
                        {LocalProvider('import_json')}
                    </button>
                    <button className="system-button size-s text" onClick={this.exportJson}>
                        <i className="far fa-arrow-down-to-bracket" />
                        {LocalProvider('export_json')}
                    </button>
                    {this.props.rightHeader}
                </div>
                <div className="page-panel-body">
                    <Row>
                        {this.props.showEditor && (
                            <Col span={8}>
                                <AceEditor
                                    className="pretty-editor"
                                    mode="json"
                                    data={JSON.stringify(schema, null, 2)}
                                    onChange={this.handleParams}
                                />
                            </Col>
                        )}
                        <Col span={this.props.showEditor ? 16 : 24} className="wrapper object-style">
                            <SchemaArrayChild
                                customDuplicateFieldButton={this.props.customDuplicateFieldButton}
                                customAddFieldButton={this.props.customAddFieldButton}
                                showSimpleViewButton={this.props.showSimpleViewButton}
                                renderAdvancedModal={this.props.renderAdvancedModal}
                                customDescriptionButton={this.props.customDescriptionButton}
                                customSettingButton={this.props.customSettingButton}
                                databases={this.props.databases}
                                items={this.state.schema}
                                onChangeData={this.onChangeSchema}
                                incrementIndex={incrementIndex}
                                mineTypeList={this.props.mineTypeList || []}
                                additionalFields={this.props.additionalFields || []}
                                onChangeSettings={this.onChangeSettings}
                                settings={settings || {}}
                                triggerAdditionalValueOptions={this.props.triggerAdditionalValueOptions}
                                reservedFieldKeyRegex={reservedFieldKeyRegex}
                                lockedFieldTypeIds={this.props.lockedFieldTypeIds}
                                reservedFieldIds={this.props.reservedFieldIds}
                            />
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }

    onChangeSettings(settings) {
        if(this.props.onChangeSettings) {
            this.props.onChangeSettings(settings);
        }
    }

    onChangeSchema(data, incrementIndex) {
        const schema = this.convertChildArrayToSchema(data, incrementIndex);
        this.jsonSchema = schema;
        this.Model.changeEditorSchemaAction({value: schema});
        this.setState({schema: data});
    }

    convertChildArrayToSchema(childArray, incrementIndex = undefined) {
        let schema = {
            type: 'object',
            properties: {}
        };
        if (incrementIndex) {
            schema['$incrementIndex'] = incrementIndex;
        }
        let required = [];
        childArray.forEach(item => {
            let data = cloneObject(item.data);
            if (data.type === 'object') {
                data = this.convertChildArrayToSchema(item.children);
                if(item.data.description) {
                    data.description = item.data.description;
                }
            } else if (data.type === 'array') {
                if(data.items && (data.items.type === 'file' || data.items.type === 'catalog')) {
                    if(data.items['$id']) {
                        item.id = data.items['$id'];
                        delete data.items['$id'];
                    }
                }
                else {
                    data.items = this.convertChildArrayToSchema(item.children);
                }
            }
            schema.properties[item.key] = data;
            schema.properties[item.key]['$id'] = item.id;
            if (item.hasOwnProperty('required') && item.required) {
                required.push(item.key);
            }
        });
        if (required.length > 0) {
            schema.required = required;
        }
        return schema;
    }

    convertSchemaToChildArray(schema) {
        let list = [];
        if (schema.type === 'object') {
            Object.keys(schema.properties).forEach(key => {
                const data = schema.properties[key];
                let item = {
                    key: key,
                    data: cloneObject(data)
                };
                if (item.data.hasOwnProperty('properties') && data.type !== 'catalog') {
                    delete item.data.properties;
                }
                if (item.data.hasOwnProperty('items') && (data.type !== 'array' || !(item.data.items.type === 'file' || item.data.items.type === 'catalog'))) {
                    delete item.data.items;
                }
                if (item.data.hasOwnProperty('$id')) {
                    item.id = item.data['$id'];
                    delete item.data['$id'];
                }
                if (data.type === 'object') {
                    item.children = this.convertSchemaToChildArray(data);
                } else if (data.type === 'array') {
                    item.children = this.convertSchemaToChildArray(data.items);
                }
                if(schema.required && schema.required.indexOf(key) !== -1) {
                    item.required = true;
                }
                list.push(item);
            });
        }
        return list;
    }

    formatIdChildArray(list, id = 0) {
        list.forEach(item => {
            if (!item.id) {
                id++;
                item.id = id;
            }
            if (item.children && item.children.length > 0) {
                id = this.formatIdChildArray(item.children, id);
            }
        });
        return id;
    }
}

jsonSchema.childContextTypes = {
    getOpenValue: PropTypes.func,
    changeCustomValue: PropTypes.func,
    Model: PropTypes.object,
    isMock: PropTypes.bool
};

jsonSchema.propTypes = {
    data: PropTypes.string,
    onChange: PropTypes.func,
    showEditor: PropTypes.bool,
    isMock: PropTypes.bool,
    Model: PropTypes.object,
    readonly: PropTypes.bool,
    mineTypeList: PropTypes.array,
    additionalFields: PropTypes.array,
    onChangeSettings: PropTypes.func,
    onError: PropTypes.func,
    triggerAdditionalValueOptions: PropTypes.array,
    reservedFieldIds: PropTypes.array,
    reservedFieldKeyPattern: PropTypes.string,
    leftHeader: PropTypes.element,
    rightHeader: PropTypes.element,
    showSimpleViewButton: PropTypes.bool,
    customDescriptionButton: PropTypes.element,
    customSettingButton: PropTypes.element,
    customAddFieldButton: PropTypes.any,
    customDuplicateFieldButton: PropTypes.any,
    lockedFieldTypeIds: PropTypes.array,
    renderAdvancedModal: PropTypes.func,
};

export default connect(state => ({
    schema: state.schema.data,
    open: state.schema.open
}))(jsonSchema);
